#!/bin/sh

set -e

webapp_db_password=eleventest01
rethinkdb_password=test123

echo "::: Installing packages..."

# make apt retry package download attempts (httpredir.debian.org appears to be quite flaky)
echo "APT::Acquire::Retries \"20\";" > /etc/apt/apt.conf

# make apt use default options instead of interactive prompts
export DEBIAN_FRONTEND=noninteractive

# prevent interactive service restart prompt during openssl package installation
echo openssl libraries/restart-without-asking boolean true | debconf-set-selections

# make MySQL installation set custom root password
echo mysql-server-5.5 mysql-server/root_password password $webapp_db_password | debconf-set-selections
echo mysql-server-5.5 mysql-server/root_password_again password $webapp_db_password | debconf-set-selections

# update apt sources (adding RethinkDB PPA, and non-free for the flash plugin)
echo "deb http://download.rethinkdb.com/apt jessie main" > /etc/apt/sources.list.d/rethinkdb.list
wget -qO- http://download.rethinkdb.com/apt/pubkey.gpg | apt-key add -
cat <<EOF > /etc/apt/sources.list.d/contrib-nonfree.list
deb http://http.us.debian.org/debian jessie contrib non-free
deb-src http://http.us.debian.org/debian jessie contrib non-free

deb http://security.debian.org/ jessie/updates non-free
deb-src http://security.debian.org/ jessie/updates non-free

# jessie-updates, previously known as 'volatile'
deb http://http.us.debian.org/debian jessie-updates contrib non-free
deb-src http://http.us.debian.org/debian jessie-updates contrib non-free
deb http://ftp.us.debian.org/debian jessie-backports contrib non-free
EOF
apt-get update
apt-get upgrade -f -y -q

# install all the things
apt-get install -f -y -q build-essential git-core curl openssl libssl-dev git \
    strace valgrind htop screen vim emacs python python-pip python-dev python3-dev \
    python3-pip python3-pkg-resources python3-setuptools dos2unix nodejs \
    nodejs-legacy rethinkdb=2.3.5~0jessie nginx php5-fpm php5-cli php5-mcrypt \
    mysql-server php5-mysql libsqlite3-dev ruby ruby-dev unzip rabbitmq-server \
    xvfb arora joe dsniff debconf-utils apt-show-versions netcat libnotify-bin

# set up flash plugin and RabbitMQ for the spritesheet generator
apt-get install -y flashplugin-nonfree > /dev/null 2>&1

# workaround for the broken Debian flashplugin-nonfree package
# see https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=851066
wget -q -P /tmp "https://fpdownload.adobe.com/get/flashplayer/pdc/28.0.0.161/flash_player_npapi_linux.x86_64.tar.gz"
tar zxfO /tmp/flash_player_npapi_linux.x86_64.tar.gz \
    libflashplayer.so > /usr/lib/flashplugin-nonfree/libflashplayer.so
chmod 644 /usr/lib/flashplugin-nonfree/libflashplayer.so
update-alternatives --install /usr/lib/mozilla/plugins/flash-mozilla.so \
    flash-mozilla.so /usr/lib/flashplugin-nonfree/libflashplayer.so 50

rabbitmq-plugins enable rabbitmq_management
/etc/init.d/rabbitmq-server restart


echo "::: Installing nvm, node, npm and some global packages"

# install nvm for user vagrant
su -l vagrant -c "curl -s -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash"

# set node v6 as default node version for user vagrant
su -l vagrant -c "(. ~/.nvm/nvm.sh && nvm install 6 2> /dev/null && nvm alias default 6)"

# install some useful npm packages globally
su -l vagrant -c "(. ~/.nvm/nvm.sh && npm install -g bunyan mocha json gulp)"


echo "::: Installing and setting up RethinkDB..."

# add RethinkDB Python driver (for command line tools)
pip3 install rethinkdb==2.3.0

# set up RethinkDB as systemd service
# see http://www.rethinkdb.com/docs/start-on-startup/
echo "d /run/rethinkdb 0755 rethinkdb rethinkdb -" > /usr/lib/tmpfiles.d/rethinkdb.conf
chmod 644 /usr/lib/tmpfiles.d/rethinkdb.conf
mkdir -p /usr/lib/systemd/system
cp /vagrant/eleven-vagrant/rethinkdb@.service /usr/lib/systemd/system/
chmod 644 /usr/lib/systemd/system/rethinkdb@.service

# initialize data and pid file directories
mkdir -p /var/lib/rethinkdb
rethinkdb create -d /var/lib/rethinkdb/eleven
chown -R rethinkdb:rethinkdb /var/lib/rethinkdb
mkdir -p /var/run/rethinkdb
chown -R rethinkdb:rethinkdb /var/run/rethinkdb

# configure RethinkDB instance and start it up
cp /vagrant/eleven-vagrant/rethinkdb.conf /etc/rethinkdb/instances.d/eleven.conf
systemctl enable rethinkdb@eleven
systemctl start rethinkdb@eleven
echo "Waiting for RethinkDB startup..."
while ! nc -z -w 1 localhost 28015; do sleep 1; done

# set admin password and make it available for fixtures management scripts
python3 -c "import rethinkdb as r;\
conn = r.connect(host='localhost', port=28015);\
r.db('rethinkdb').table('users').get('admin').update({'password': '$rethinkdb_password'}).run(conn)"

echo $rethinkdb_password > /root/.rethinkdbpw
echo $rethinkdb_password > /home/vagrant/.rethinkdbpw
chown vagrant:vagrant /home/vagrant/.rethinkdbpw


echo "::: Importing Eleven fixtures into RethinkDB..."

(cd /vagrant/eleven-fixtures-rdb && ./rdb-import.sh --dbname=eleven_dev --from=rdb-fixtures --noprogress)


echo "::: (Re)building binary modules for Eleven node.js components..."
if [ -d /vagrant/eleven-throwaway-server ]; then
	su -l vagrant -c "(. ~/.nvm/nvm.sh && cd /vagrant/eleven-throwaway-server/ && nvm install 2> /dev/null && npm rebuild --no-bin-links)"
fi
if [ -d /vagrant/eleven-server ]; then
	su -l vagrant -c "(. ~/.nvm/nvm.sh && cd /vagrant/eleven-server/ && nvm install 2> /dev/null && npm install --no-bin-links --no-progress)"
	su -l vagrant -c "(. ~/.nvm/nvm.sh && cd /vagrant/eleven-server/ && npm -s run preproc)"
	if [ ! -f /vagrant/eleven-server/config_local.js ]; then
		cp /vagrant/eleven-server/config_local.js.SAMPLE_VAGRANT /vagrant/eleven-server/config_local.js
	fi
fi
if [ -d /vagrant/eleven-http-api ]; then
	su -l vagrant -c "(. ~/.nvm/nvm.sh && cd /vagrant/eleven-http-api/ && nvm install 2> /dev/null && npm install --no-bin-links --no-progress)"
	if [ ! -f /vagrant/eleven-http-api/config/local.json ]; then
		cp /vagrant/eleven-http-api/config/local.json.SAMPLE_VAGRANT /vagrant/eleven-http-api/config/local.json
	fi
fi


echo "::: Setting up local asset server..."

# nginx setup for assets server
cp /vagrant/eleven-vagrant/nginx-assets /etc/nginx/sites-available/assets
ln -s /etc/nginx/sites-available/assets /etc/nginx/sites-enabled


echo "::: Setting up spritesheet generator..."

if [ -d /vagrant/eleven-spritesheet-generator ]; then
	pip install virtualenv
	su -l vagrant -c "(cd /vagrant/eleven-spritesheet-generator && virtualenv venv --always-copy && source venv/bin/activate && pip install .)"
	if [ ! -f /vagrant/eleven-spritesheet-generator/config.py ]; then
		cp /vagrant/eleven-spritesheet-generator/config.py.sample /vagrant/eleven-spritesheet-generator/config.py
	fi
	# set up as systemd service
	cp /vagrant/eleven-spritesheet-generator/deploy/eleven-spritesheet-generator.service.VAGRANT /usr/lib/systemd/system/eleven-spritesheet-generator.service
	chmod 644 /usr/lib/systemd/system/eleven-spritesheet-generator.service
	systemctl enable eleven-spritesheet-generator
	systemctl start eleven-spritesheet-generator
fi


echo "::: Running Webapp Setup..."

# copy configuration files for nginx and PHP, and the local environment settings for the webapp
cp /vagrant/eleven-vagrant/nginx-webapp /etc/nginx/sites-available/webapp
ln -s /etc/nginx/sites-available/webapp /etc/nginx/sites-enabled
cp /vagrant/eleven-vagrant/php.ini /etc/php5/fpm/php.ini
[ ! -f /vagrant/eleven-web/.env ] && cp /vagrant/eleven-web/.env.example /vagrant/eleven-web/.env

# Disable "sendfile" in nginx - apparently it causes problems when running in
# Vagrant/VirtualBox, causing encoding issues that make it seem that your
# CSS/JS/static file changes are permanently stuck in cache.
#
# Info about this issue: http://jeremyfelt.com/code/2013/01/08/clear-nginx-cache-in-vagrant/
sed -i 's/sendfile on/sendfile off/g' /etc/nginx/nginx.conf

# restart nginx and php5-fpm
rm /etc/nginx/sites-enabled/default
/usr/sbin/service php5-fpm restart
/usr/sbin/service nginx restart

mkdir -p /var/eleven/eleven-web/cache
mkdir -p /var/eleven/eleven-web/logs
mkdir -p /var/eleven/eleven-web/meta
mkdir -p /var/eleven/eleven-web/sessions
mkdir -p /var/eleven/eleven-web/views
chown -R www-data.www-data /var/eleven/eleven-web

# install Composer (PHP package manager)
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

# run composer to install the Laravel framework and other webapp dependencies
(cd /vagrant/eleven-web/ && composer install --prefer-source --no-interaction --no-progress)

# setup webapp database
mysql -u root -p$webapp_db_password -e 'CREATE DATABASE elevenweb;'
mysql -u root -p$webapp_db_password -e "CREATE USER 'eleven'@'localhost' IDENTIFIED BY 'eleventest@'"
mysql -u root -p$webapp_db_password -e "GRANT ALL PRIVILEGES ON *.* TO 'eleven'@'localhost'"

# populate webapp database - run migrations and load initial data
(cd /vagrant/eleven-web/ && php artisan migrate)
(cd /vagrant/eleven-web/ && php artisan db:seed)
(cd /vagrant/eleven-web/ && php artisan db:seed --class=DevAccountsSeeder)

# set permissions and give the webserver access to the eleven-web folder
usermod -a -G vagrant www-data
chmod -R 777 /vagrant/eleven-web/storage/

# run gulp to process assets
su -l vagrant -c "(. ~/.nvm/nvm.sh && cd /vagrant/eleven-web/ && npm install --no-bin-links --no-progress ; npm install --no-bin-links --no-progress && npm rebuild --no-bin-links --no-progress)"
su -l vagrant -c "(. ~/.nvm/nvm.sh && cd /vagrant/eleven-web/ &&  gulp --production)"


echo "::: Setting up Vanilla Forum..."

# Vanilla core
mkdir -p /var/www/forum
git clone https://github.com/vanilla/vanilla.git -b Vanilla_2.1.13p1 --single-branch --depth 1 -c advice.detachedHead=false /var/www/forum

# jsConnect plugin
wget -q -P /var/www/forum/plugins http://cdn.vanillaforums.com/www.vanillaforums.org/addons/2748HPRKK41Y.zip
unzip /var/www/forum/plugins/2748HPRKK41Y.zip -d /var/www/forum/plugins

# jsConnect auto-signin plugin
wget -q -P /var/www/forum/plugins http://cdn.vanillaforums.com/www.vanillaforums.org/addons/EEUMKPCDYY38.zip
unzip /var/www/forum/plugins/EEUMKPCDYY38.zip -d /var/www/forum/plugins

# Copy forum theme
if [ -d /vagrant/eleven-forum-theme ]; then
	rsync -r --exclude=.git* /vagrant/eleven-forum-theme/ /var/www/forum/themes/eleven
fi

# Copy config
cp /vagrant/eleven-vagrant/vanilla-config.php /var/www/forum/conf/config.php

# Set permissions for forum
chmod -R 777 /var/www/forum/conf
chmod -R 777 /var/www/forum/uploads
chmod -R 777 /var/www/forum/cache

# Set up test forum DB
mysql -u root -p$webapp_db_password -e 'CREATE DATABASE elevenforum;'
mysql -u root -p$webapp_db_password < /vagrant/eleven-vagrant/eleven-forum-testdata.sql


echo "::: Setting up MailCatcher (local email testing tool)..."

# install MailCatcher (for testing outbound emails)
gem install mailcatcher


echo "::: Setting up monitoring components..."

# set up statsd, graphite and collectd
/vagrant/eleven-vagrant/add-statsd-graphite-collectd.sh


echo "::: Putting on the finishing touches..."

/usr/bin/updatedb

# source optional shell profile customization in .profile
cat >> /home/vagrant/.profile <<EOF

# include personal profile from shared folder if there is one
if [ -f "/vagrant/vagrant-profile.sh" ]; then
    . "/vagrant/vagrant-profile.sh"
fi
EOF

echo "::: Eleven development VM setup completed!"
