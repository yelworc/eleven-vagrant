# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
	# All Vagrant configuration is done here. The most common configuration
	# options are documented and commented below. For a complete reference,
	# please see the online documentation at vagrantup.com.

	config.vm.define "eleven", primary: true do |eleven|
		eleven.vm.box = "debian/contrib-jessie64"
		eleven.vm.box_version = "8.10.0"
		eleven.vm.provision "shell", path: "./eleven-vagrant/setup-debian8.sh"
		eleven.vm.network :private_network, ip: "192.168.23.23"
		eleven.vm.provider :virtualbox do |vb|
			vb.customize ["modifyvm", :id, "--memory", "1536"]
			vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
		end
	end

	config.vm.define "webapp", autostart: false do |webapp|
		webapp.vm.box = "eleven-dev"
		webapp.vm.box_url = "http://puppet-vagrant-boxes.puppetlabs.com/debian-73-x64-virtualbox-puppet.box"
		webapp.vm.provision "shell", path: "./eleven-vagrant/setup-debian73-webapp.sh"
		webapp.vm.network :private_network, ip: "192.168.23.24"
		webapp.vm.provider :virtualbox do |vb|
			vb.customize ["modifyvm", :id, "--memory", "1024"]
		end
	end

	config.vm.define "omnios", autostart: false do |omnios|
		omnios.vm.box = "eleven-omnios"
		omnios.vm.box_url = "http://omnios.omniti.com/media/omnios-latest.box"
		omnios.vm.provision "shell", path: "./eleven-vagrant/setup-omnios.sh"
		omnios.vm.network :private_network, ip: "192.168.23.24"
		config.vm.network :forwarded_port, guest: 22, host: 2223, id: "ssh", auto_correct: true
		omnios.vm.provider :virtualbox do |vb|
			vb.customize ["modifyvm", :id, "--memory", "1024"]
		end
	end


	# Create a forwarded port mapping which allows access to a specific port
	# within the machine from a port on the host machine. In the example below,
	# accessing "localhost:8080" will access port 80 on the guest machine.
	# config.vm.network :forwarded_port, guest: 80, host: 8080
	#~ config.vm.network :forwarded_port, guest: 8000, host: 8000
	#~ config.vm.network :forwarded_port, guest: 1443, host: 1443
	#~ config.vm.network :forwarded_port, guest: 1843, host: 1843

	# Create a public network, which generally matched to bridged network.
	# Bridged networks make the machine appear as another physical device on
	# your network.
	#~ config.vm.network :public_network

	# If true, then any SSH connections made will enable agent forwarding.
	# Default value: false
	# config.ssh.forward_agent = true

	# Share an additional folder to the guest VM. The first argument is
	# the path on the host to the actual folder. The second argument is
	# the path on the guest to mount the folder. And the optional third
	# argument is a set of non-required options.
	# config.vm.synced_folder "../data", "/vagrant_data"

	# restore pre-v1.7 behavior of reusing the same SSH key for all installations
	# (because using the dynamically generated key is a pain on Windows)
	# see https://stackoverflow.com/questions/28471542
	config.ssh.insert_key = false
end
