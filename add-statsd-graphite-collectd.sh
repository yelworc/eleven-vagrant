#!/bin/sh

set -e

##
# statsd
#

# install statsd
dpkg -i /vagrant/eleven-vagrant/statsd/statsd_0.7.2-snapshot-20141203_all.deb

# copy customized configuration and restart
cp /vagrant/eleven-vagrant/statsd/localConfig.js /etc/statsd/localConfig.js
/etc/init.d/statsd restart


##
# collectd
#

# install collectd
apt-get -qq -y install collectd collectd-utils

# copy config and restart
cp /vagrant/eleven-vagrant/collectd/collectd.conf /etc/collectd/collectd.conf
/etc/init.d/collectd restart


##
# graphite
#

# install prerequisites (specific old version of Django, because graphite-web
# 0.9.13 fails to load with more recent ones, like the regular Debian package)
apt-get -qq -y install uwsgi uwsgi-plugin-python python-cairo python-twisted \
    python-memcache python-pysqlite2 python-simplejson
pip install django==1.5.12
pip install django-tagging==0.3.2

# install Graphite components
pip install whisper==0.9.13
pip install carbon==0.9.13
pip install graphite-web==0.9.13

# copy and enable nginx config
cp /vagrant/eleven-vagrant/graphite/graphite-nginx.conf /etc/nginx/sites-available/graphite
ln -s /etc/nginx/sites-available/graphite /etc/nginx/sites-enabled/

# copy and enable uWSGI app config
cp /vagrant/eleven-vagrant/graphite/graphite-uwsgi.ini /etc/uwsgi/apps-available/graphite.ini
ln -s /etc/uwsgi/apps-available/graphite.ini /etc/uwsgi/apps-enabled/

# set up webapp config files (mostly just copying the preset examples for now)
cp /opt/graphite/conf/dashboard.conf.example /opt/graphite/conf/dashboard.conf
cp /opt/graphite/conf/graphTemplates.conf.example /opt/graphite/conf/graphTemplates.conf
cp /opt/graphite/conf/graphite.wsgi.example /opt/graphite/conf/wsgi.py
cp /vagrant/eleven-vagrant/graphite/webapp-local_settings.py /opt/graphite/webapp/graphite/local_settings.py

# create the local database for the webapp
python /opt/graphite/webapp/graphite/manage.py syncdb --noinput

# create system user for carbon-cache and webapp, give appropriate permissions
adduser --system --group graphite
chown -R graphite:graphite /opt/graphite

# configure carbon-cache to run as a service
cp /opt/graphite/conf/carbon.conf.example /opt/graphite/conf/carbon.conf
cp /vagrant/eleven-vagrant/graphite/carbon-cache.service /etc/systemd/system/carbon-cache.service
systemctl enable carbon-cache

# copy carbon-cache data retention/aggregation config files
cp /vagrant/eleven-vagrant/graphite/storage-schemas.conf /opt/graphite/conf/storage-schemas.conf
cp /vagrant/eleven-vagrant/graphite/storage-aggregation.conf /opt/graphite/conf/storage-aggregation.conf

# start things up (resp. restart to reload config)
systemctl start carbon-cache
systemctl restart uwsgi
systemctl restart nginx
