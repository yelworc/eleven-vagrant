#!/bin/sh

# install development related packages (see <http://omnios.omniti.com/wiki.php/DevEnv>)
pkg install -q \
	developer/gcc48 \
	developer/object-file \
	developer/linker \
	developer/library/lint \
	developer/build/gnu-make \
	developer/gnu-binutils \
	system/header \
	system/library/math/header-math \
	terminal/tmux \
	developer/versioning/git \
	editor/nano

# fix gcc/gnu binutils paths
ln -s /opt/gcc-4.8.1/bin/gcc /bin/cc
ln -s /opt/gcc-4.8.1/bin/gcc /bin/gcc
ln -s /opt/gcc-4.8.1/bin/g++ /bin/g++
ln -s /usr/gnu/i386-pc-solaris2.11/bin/objdump /bin/objdump

# clone and build node.js
# v0.10.20 because in later versions the node ustack helper for DTrace is too big (see <https://stackoverflow.com/questions/23848979/>)
git clone https://github.com/joyent/node.git
cd node
git checkout v0.10.20
./configure --with-dtrace --dest-cpu=x64
gmake CXXFLAGS+="-ffunction-sections -fdata-sections"
gmake install
ln -s /usr/local/bin/node /bin/node
ln -s /usr/local/bin/npm /bin/npm
cd ~

# add some useful modules globally
/bin/npm install -g supervisor
MAKE=gmake /bin/npm install -g bunyan
/bin/npm install -g mocha
MAKE=gmake /bin/npm install -g stackvis

# command line tools for global node modules are installed to /usr/local/bin
# by default, so add that to $PATH
echo 'PATH=$PATH:/usr/local/bin' >> /export/home/vagrant/.profile
