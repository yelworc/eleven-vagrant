#!/bin/sh

echo ::: Installing packages...

# prevent interactive service restart prompt during openssl package installation
echo openssl libraries/restart-without-asking boolean true | sudo debconf-set-selections

# prevent interactive password prompt during mysql-server package installation
echo mysql-server-5.5 mysql-server/root_password password eleventest01 | debconf-set-selections
echo mysql-server-5.5 mysql-server/root_password_again password eleventest01 | debconf-set-selections

# update apt sources (adding RethinkDB PPA and backports repo for node.js)
echo "deb http://ftp.us.debian.org/debian wheezy-backports main" >> /etc/apt/sources.list
apt-get update

# install all the things
apt-get install -qq -f -y build-essential git-core curl openssl libssl-dev git strace valgrind htop screen vim emacs python-pip python-pkg-resources python-setuptools dos2unix nodejs nodejs-legacy nginx php5-fpm php5-cli php5-mcrypt mysql-server php5-mysql libsqlite3-dev ruby1.9.3

# Temporarily disabled npm install in webapp VM, may be needed later if we want to run node.js-based utilities
# install npm and add some global modules (clean=no to prevent interactive
# prompt to remove old npm versions)
#curl --location --silent https://npmjs.org/install.sh | clean=no /bin/sh
#/usr/bin/npm install -g supervisor
#/usr/bin/npm install -g bunyan
#/usr/bin/npm install -g mocha

echo ::: Setting up nginx and PHP configuration

# copy configuration files for nginx and PHP, and the local environment settings for the webapp
cp /vagrant/eleven-vagrant/nginx-webapp /etc/nginx/sites-available/webapp
ln -s /etc/nginx/sites-available/webapp /etc/nginx/sites-enabled
cp /vagrant/eleven-vagrant/php.ini /etc/php5/fpm/php.ini
[ ! -f /vagrant/eleven-web/.env ] && cp /vagrant/eleven-web/.env.example /vagrant/eleven-web/.env

# Disable "sendfile" in nginx - apparently it causes problems when running in
# Vagrant/VirtualBox, causing encoding issues that make it seem that your
# CSS/JS/static file changes are permanently stuck in cache.
#
# Info about this issue: http://jeremyfelt.com/code/2013/01/08/clear-nginx-cache-in-vagrant/
sed -i 's/sendfile on/sendfile off/g' /etc/nginx/nginx.conf

# restart nginx and php5-fpm
rm /etc/nginx/sites-enabled/default
/usr/sbin/service php5-fpm restart
/usr/sbin/service nginx restart

echo ::: Setting up MailCatcher local email testing tool

# install MailCatcher (for testing outbound emails)
gem install mailcatcher

echo ::: Creating storage directory
mkdir -p /var/eleven/eleven-web/cache
mkdir -p /var/eleven/eleven-web/logs
mkdir -p /var/eleven/eleven-web/meta
mkdir -p /var/eleven/eleven-web/sessions
mkdir -p /var/eleven/eleven-web/views
chown -R www-data.www-data /var/eleven/eleven-web

echo ::: Installing Composer

# install Composer
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

echo ::: Installing dependencies for Eleven webapp

# run composer to install the Laravel framework and other webapp dependencies
(cd /vagrant/eleven-web/ && composer install --prefer-source --no-interaction --no-progress)

echo ::: Setting up MySQL database and loading initial data

# setup webapp database
mysql -u root -peleventest01 -e 'CREATE DATABASE elevenweb;'
mysql -u root -peleventest01 -e "CREATE USER 'eleven'@'localhost' IDENTIFIED BY 'eleventest@'"
mysql -u root -peleventest01 -e "GRANT ALL PRIVILEGES ON *.* TO 'eleven'@'localhost'"

# populate webapp database - run migrations and load initial data
(cd /vagrant/eleven-web/ && php artisan migrate)
(cd /vagrant/eleven-web/ && php artisan db:seed)

echo ::: Setting permissions

# set permissions and give the webserver access to the eleven-web folder
usermod -a -G vagrant www-data
chmod -R 777 /vagrant/eleven-web/storage/

# source optional shell profile customization in .profile
cat >> /home/vagrant/.profile <<EOF

# include personal profile from shared folder if there is one
if [ -f "/vagrant/vagrant-profile.sh" ]; then
    . "/vagrant/vagrant-profile.sh"
fi
EOF

/usr/bin/updatedb

echo ::: Webapp VM setup completed!
