CREATE DATABASE  IF NOT EXISTS `elevenforum` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `elevenforum`;
-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: elevenforum
-- ------------------------------------------------------
-- Server version	5.5.38-0+wheezy1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `GDN_Activity`
--

DROP TABLE IF EXISTS `GDN_Activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_Activity` (
  `ActivityID` int(11) NOT NULL AUTO_INCREMENT,
  `ActivityTypeID` int(11) NOT NULL,
  `NotifyUserID` int(11) NOT NULL DEFAULT '0',
  `ActivityUserID` int(11) DEFAULT NULL,
  `RegardingUserID` int(11) DEFAULT NULL,
  `Photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HeadlineFormat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Story` text COLLATE utf8_unicode_ci,
  `Format` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RecordType` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RecordID` int(11) DEFAULT NULL,
  `InsertUserID` int(11) DEFAULT NULL,
  `DateInserted` datetime NOT NULL,
  `InsertIPAddress` varchar(39) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `Notified` tinyint(4) NOT NULL DEFAULT '0',
  `Emailed` tinyint(4) NOT NULL DEFAULT '0',
  `Data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ActivityID`),
  KEY `IX_Activity_Notify` (`NotifyUserID`,`Notified`),
  KEY `IX_Activity_Recent` (`NotifyUserID`,`DateUpdated`),
  KEY `IX_Activity_Feed` (`NotifyUserID`,`ActivityUserID`,`DateUpdated`),
  KEY `IX_Activity_DateUpdated` (`DateUpdated`),
  KEY `FK_Activity_InsertUserID` (`InsertUserID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_Activity`
--

LOCK TABLES `GDN_Activity` WRITE;
/*!40000 ALTER TABLE `GDN_Activity` DISABLE KEYS */;
INSERT INTO `GDN_Activity` VALUES (1,17,-1,1,NULL,NULL,'{ActivityUserID,You} joined.','Welcome Aboard!',NULL,NULL,NULL,NULL,NULL,'2014-09-14 01:24:37','192.168.23.1','2014-09-14 01:24:37',0,0,'a:0:{}'),(2,15,-1,1,2,NULL,'{RegardingUserID,you} &rarr; {ActivityUserID,you}','Ping! An activity post is a public way to talk at someone. When you update your status here, it posts it on your activity feed.','Html',NULL,NULL,NULL,2,'2014-09-14 01:24:43',NULL,'2014-09-14 01:24:43',0,0,NULL);
/*!40000 ALTER TABLE `GDN_Activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_ActivityComment`
--

DROP TABLE IF EXISTS `GDN_ActivityComment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_ActivityComment` (
  `ActivityCommentID` int(11) NOT NULL AUTO_INCREMENT,
  `ActivityID` int(11) NOT NULL,
  `Body` text COLLATE utf8_unicode_ci NOT NULL,
  `Format` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `InsertUserID` int(11) NOT NULL,
  `DateInserted` datetime NOT NULL,
  `InsertIPAddress` varchar(39) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ActivityCommentID`),
  KEY `FK_ActivityComment_ActivityID` (`ActivityID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_ActivityComment`
--

LOCK TABLES `GDN_ActivityComment` WRITE;
/*!40000 ALTER TABLE `GDN_ActivityComment` DISABLE KEYS */;
/*!40000 ALTER TABLE `GDN_ActivityComment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_ActivityType`
--

DROP TABLE IF EXISTS `GDN_ActivityType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_ActivityType` (
  `ActivityTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `AllowComments` tinyint(4) NOT NULL DEFAULT '0',
  `ShowIcon` tinyint(4) NOT NULL DEFAULT '0',
  `ProfileHeadline` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FullHeadline` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RouteCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Notify` tinyint(4) NOT NULL DEFAULT '0',
  `Public` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ActivityTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_ActivityType`
--

LOCK TABLES `GDN_ActivityType` WRITE;
/*!40000 ALTER TABLE `GDN_ActivityType` DISABLE KEYS */;
INSERT INTO `GDN_ActivityType` VALUES (1,'SignIn',0,0,'%1$s signed in.','%1$s signed in.',NULL,0,1),(2,'Join',1,0,'%1$s joined.','%1$s joined.',NULL,0,1),(3,'JoinInvite',1,0,'%1$s accepted %4$s invitation for membership.','%1$s accepted %4$s invitation for membership.',NULL,0,1),(4,'JoinApproved',1,0,'%1$s approved %4$s membership application.','%1$s approved %4$s membership application.',NULL,0,1),(5,'JoinCreated',1,0,'%1$s created an account for %3$s.','%1$s created an account for %3$s.',NULL,0,1),(6,'AboutUpdate',1,0,'%1$s updated %6$s profile.','%1$s updated %6$s profile.',NULL,0,1),(7,'WallComment',1,1,'%1$s wrote:','%1$s wrote on %4$s %5$s.',NULL,0,1),(8,'PictureChange',1,0,'%1$s changed %6$s profile picture.','%1$s changed %6$s profile picture.',NULL,0,1),(9,'RoleChange',1,0,'%1$s changed %4$s permissions.','%1$s changed %4$s permissions.',NULL,1,1),(10,'ActivityComment',0,1,'%1$s','%1$s commented on %4$s %8$s.','activity',1,1),(11,'Import',0,0,'%1$s imported data.','%1$s imported data.',NULL,1,0),(12,'Banned',0,0,'%1$s banned %3$s.','%1$s banned %3$s.',NULL,0,1),(13,'Unbanned',0,0,'%1$s un-banned %3$s.','%1$s un-banned %3$s.',NULL,0,1),(14,'Applicant',0,0,'%1$s applied for membership.','%1$s applied for membership.',NULL,1,0),(15,'WallPost',1,1,'%3$s wrote:','%3$s wrote on %2$s %5$s.',NULL,0,1),(16,'Default',0,0,NULL,NULL,NULL,0,1),(17,'Registration',0,0,NULL,NULL,NULL,0,1),(18,'Status',0,0,NULL,NULL,NULL,0,1),(19,'Ban',0,0,NULL,NULL,NULL,0,1),(20,'ConversationMessage',0,0,'%1$s sent you a %8$s.','%1$s sent you a %8$s.','message',1,0),(21,'AddedToConversation',0,0,'%1$s added %3$s to a %8$s.','%1$s added %3$s to a %8$s.','conversation',1,0),(22,'NewDiscussion',0,0,'%1$s started a %8$s.','%1$s started a %8$s.','discussion',0,0),(23,'NewComment',0,0,'%1$s commented on a discussion.','%1$s commented on a discussion.','discussion',0,0),(24,'DiscussionComment',0,0,'%1$s commented on %4$s %8$s.','%1$s commented on %4$s %8$s.','discussion',1,0),(25,'DiscussionMention',0,0,'%1$s mentioned %3$s in a %8$s.','%1$s mentioned %3$s in a %8$s.','discussion',1,0),(26,'CommentMention',0,0,'%1$s mentioned %3$s in a %8$s.','%1$s mentioned %3$s in a %8$s.','comment',1,0),(27,'BookmarkComment',0,0,'%1$s commented on your %8$s.','%1$s commented on your %8$s.','bookmarked discussion',1,0),(28,'Discussion',0,0,NULL,NULL,NULL,0,1),(29,'Comment',0,0,NULL,NULL,NULL,0,1);
/*!40000 ALTER TABLE `GDN_ActivityType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_AnalyticsLocal`
--

DROP TABLE IF EXISTS `GDN_AnalyticsLocal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_AnalyticsLocal` (
  `TimeSlot` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `Views` int(11) DEFAULT NULL,
  `EmbedViews` int(11) DEFAULT NULL,
  UNIQUE KEY `UX_AnalyticsLocal` (`TimeSlot`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_AnalyticsLocal`
--

LOCK TABLES `GDN_AnalyticsLocal` WRITE;
/*!40000 ALTER TABLE `GDN_AnalyticsLocal` DISABLE KEYS */;
INSERT INTO `GDN_AnalyticsLocal` VALUES ('20140914',59,0);
/*!40000 ALTER TABLE `GDN_AnalyticsLocal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_Ban`
--

DROP TABLE IF EXISTS `GDN_Ban`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_Ban` (
  `BanID` int(11) NOT NULL AUTO_INCREMENT,
  `BanType` enum('IPAddress','Name','Email') COLLATE utf8_unicode_ci NOT NULL,
  `BanValue` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CountUsers` int(10) unsigned NOT NULL DEFAULT '0',
  `CountBlockedRegistrations` int(10) unsigned NOT NULL DEFAULT '0',
  `InsertUserID` int(11) NOT NULL,
  `DateInserted` datetime NOT NULL,
  PRIMARY KEY (`BanID`),
  UNIQUE KEY `UX_Ban` (`BanType`,`BanValue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_Ban`
--

LOCK TABLES `GDN_Ban` WRITE;
/*!40000 ALTER TABLE `GDN_Ban` DISABLE KEYS */;
/*!40000 ALTER TABLE `GDN_Ban` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_Category`
--

DROP TABLE IF EXISTS `GDN_Category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_Category` (
  `CategoryID` int(11) NOT NULL AUTO_INCREMENT,
  `ParentCategoryID` int(11) DEFAULT NULL,
  `TreeLeft` int(11) DEFAULT NULL,
  `TreeRight` int(11) DEFAULT NULL,
  `Depth` int(11) DEFAULT NULL,
  `CountDiscussions` int(11) NOT NULL DEFAULT '0',
  `CountComments` int(11) NOT NULL DEFAULT '0',
  `DateMarkedRead` datetime DEFAULT NULL,
  `AllowDiscussions` tinyint(4) NOT NULL DEFAULT '1',
  `Archived` tinyint(4) NOT NULL DEFAULT '0',
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `UrlCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Sort` int(11) DEFAULT NULL,
  `CssClass` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PermissionCategoryID` int(11) NOT NULL DEFAULT '-1',
  `HideAllDiscussions` tinyint(4) NOT NULL DEFAULT '0',
  `DisplayAs` enum('Categories','Discussions','Default') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Default',
  `InsertUserID` int(11) NOT NULL,
  `UpdateUserID` int(11) DEFAULT NULL,
  `DateInserted` datetime NOT NULL,
  `DateUpdated` datetime NOT NULL,
  `LastCommentID` int(11) DEFAULT NULL,
  `LastDiscussionID` int(11) DEFAULT NULL,
  `LastDateInserted` datetime DEFAULT NULL,
  PRIMARY KEY (`CategoryID`),
  KEY `FK_Category_InsertUserID` (`InsertUserID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_Category`
--

LOCK TABLES `GDN_Category` WRITE;
/*!40000 ALTER TABLE `GDN_Category` DISABLE KEYS */;
INSERT INTO `GDN_Category` VALUES (-1,-1,1,14,0,0,0,NULL,1,0,'Root','root','Root of category tree. Users should never see this.',1,NULL,NULL,-1,0,'Default',1,1,'2014-09-14 01:24:42','2014-09-14 01:24:42',NULL,NULL,NULL),(1,-1,4,5,1,1,1,NULL,1,0,'General','general','General discussions',4,NULL,NULL,-1,0,'Default',1,1,'2014-09-14 01:24:42','2014-09-14 01:24:42',1,1,NULL),(2,-1,2,3,1,0,0,NULL,1,0,'Announcements and Events','announcements','',2,'',NULL,-1,0,'Default',1,1,'2014-09-14 05:28:24','2014-09-14 05:28:24',NULL,NULL,NULL),(3,-1,6,7,1,0,0,NULL,1,0,'Ideas','ideas','',6,'',NULL,-1,0,'Default',1,1,'2014-09-14 05:28:41','2014-09-14 05:28:41',NULL,NULL,NULL),(4,-1,8,9,1,0,0,NULL,1,0,'Marketplace','marketplace','',8,'',NULL,-1,0,'Default',1,1,'2014-09-14 05:28:59','2014-09-14 05:28:59',NULL,NULL,NULL),(5,-1,10,11,1,1,0,NULL,1,0,'Bugs','bugs','',10,'',NULL,-1,0,'Default',1,1,'2014-09-14 05:33:38','2014-09-14 05:33:38',NULL,2,'2014-09-14 05:34:51'),(6,-1,12,13,1,0,0,NULL,1,0,'Off Topic','off-topic','',12,'',NULL,-1,0,'Default',1,1,'2014-09-14 05:33:58','2014-09-14 05:33:58',NULL,NULL,NULL);
/*!40000 ALTER TABLE `GDN_Category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_Comment`
--

DROP TABLE IF EXISTS `GDN_Comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_Comment` (
  `CommentID` int(11) NOT NULL AUTO_INCREMENT,
  `DiscussionID` int(11) NOT NULL,
  `InsertUserID` int(11) DEFAULT NULL,
  `UpdateUserID` int(11) DEFAULT NULL,
  `DeleteUserID` int(11) DEFAULT NULL,
  `Body` text COLLATE utf8_unicode_ci NOT NULL,
  `Format` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateInserted` datetime DEFAULT NULL,
  `DateDeleted` datetime DEFAULT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `InsertIPAddress` varchar(39) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UpdateIPAddress` varchar(39) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Flag` tinyint(4) NOT NULL DEFAULT '0',
  `Score` float DEFAULT NULL,
  `Attributes` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`CommentID`),
  KEY `IX_Comment_1` (`DiscussionID`,`DateInserted`),
  KEY `IX_Comment_DateInserted` (`DateInserted`),
  KEY `FK_Comment_InsertUserID` (`InsertUserID`),
  FULLTEXT KEY `TX_Comment` (`Body`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_Comment`
--

LOCK TABLES `GDN_Comment` WRITE;
/*!40000 ALTER TABLE `GDN_Comment` DISABLE KEYS */;
INSERT INTO `GDN_Comment` VALUES (1,1,2,NULL,NULL,'This is the first comment on your site and it&rsquo;s an important one. \n\nDon&rsquo;t see your must-have feature? We keep Vanilla nice and simple by default. Use <b>addons</b> to get the special sauce your community needs.\n\nNot sure which addons to enable? Our favorites are Button Bar and Tagging. They&rsquo;re almost always a great start.','Html','2014-09-14 01:24:43',NULL,NULL,NULL,NULL,0,NULL,NULL);
/*!40000 ALTER TABLE `GDN_Comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_Conversation`
--

DROP TABLE IF EXISTS `GDN_Conversation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_Conversation` (
  `ConversationID` int(11) NOT NULL AUTO_INCREMENT,
  `Subject` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Contributors` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `FirstMessageID` int(11) DEFAULT NULL,
  `InsertUserID` int(11) NOT NULL,
  `DateInserted` datetime DEFAULT NULL,
  `InsertIPAddress` varchar(39) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UpdateUserID` int(11) DEFAULT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `UpdateIPAddress` varchar(39) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CountMessages` int(11) NOT NULL DEFAULT '0',
  `LastMessageID` int(11) DEFAULT NULL,
  `RegardingID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ConversationID`),
  KEY `FK_Conversation_FirstMessageID` (`FirstMessageID`),
  KEY `FK_Conversation_InsertUserID` (`InsertUserID`),
  KEY `FK_Conversation_DateInserted` (`DateInserted`),
  KEY `FK_Conversation_UpdateUserID` (`UpdateUserID`),
  KEY `IX_Conversation_RegardingID` (`RegardingID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_Conversation`
--

LOCK TABLES `GDN_Conversation` WRITE;
/*!40000 ALTER TABLE `GDN_Conversation` DISABLE KEYS */;
INSERT INTO `GDN_Conversation` VALUES (1,NULL,'a:2:{i:0;s:1:\"2\";i:1;s:1:\"1\";}',NULL,2,'2014-09-14 01:24:42',NULL,NULL,NULL,NULL,1,1,NULL);
/*!40000 ALTER TABLE `GDN_Conversation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_ConversationMessage`
--

DROP TABLE IF EXISTS `GDN_ConversationMessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_ConversationMessage` (
  `MessageID` int(11) NOT NULL AUTO_INCREMENT,
  `ConversationID` int(11) NOT NULL,
  `Body` text COLLATE utf8_unicode_ci NOT NULL,
  `Format` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `InsertUserID` int(11) DEFAULT NULL,
  `DateInserted` datetime NOT NULL,
  `InsertIPAddress` varchar(39) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`MessageID`),
  KEY `FK_ConversationMessage_ConversationID` (`ConversationID`),
  KEY `FK_ConversationMessage_InsertUserID` (`InsertUserID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_ConversationMessage`
--

LOCK TABLES `GDN_ConversationMessage` WRITE;
/*!40000 ALTER TABLE `GDN_ConversationMessage` DISABLE KEYS */;
INSERT INTO `GDN_ConversationMessage` VALUES (1,1,'Pssst. Hey. A conversation is a private chat between two or more members. No one can see it except the members added. You can delete this one since I&rsquo;m just a bot and know better than to talk back.','Html',2,'2014-09-14 01:24:42',NULL);
/*!40000 ALTER TABLE `GDN_ConversationMessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_Discussion`
--

DROP TABLE IF EXISTS `GDN_Discussion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_Discussion` (
  `DiscussionID` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ForeignID` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CategoryID` int(11) NOT NULL,
  `InsertUserID` int(11) NOT NULL,
  `UpdateUserID` int(11) DEFAULT NULL,
  `FirstCommentID` int(11) DEFAULT NULL,
  `LastCommentID` int(11) DEFAULT NULL,
  `Name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Body` text COLLATE utf8_unicode_ci NOT NULL,
  `Format` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CountComments` int(11) NOT NULL DEFAULT '0',
  `CountBookmarks` int(11) DEFAULT NULL,
  `CountViews` int(11) NOT NULL DEFAULT '1',
  `Closed` tinyint(4) NOT NULL DEFAULT '0',
  `Announce` tinyint(4) NOT NULL DEFAULT '0',
  `Sink` tinyint(4) NOT NULL DEFAULT '0',
  `DateInserted` datetime NOT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `InsertIPAddress` varchar(39) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UpdateIPAddress` varchar(39) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateLastComment` datetime DEFAULT NULL,
  `LastCommentUserID` int(11) DEFAULT NULL,
  `Score` float DEFAULT NULL,
  `Attributes` text COLLATE utf8_unicode_ci,
  `RegardingID` int(11) DEFAULT NULL,
  PRIMARY KEY (`DiscussionID`),
  KEY `IX_Discussion_Type` (`Type`),
  KEY `IX_Discussion_ForeignID` (`ForeignID`),
  KEY `IX_Discussion_DateInserted` (`DateInserted`),
  KEY `IX_Discussion_DateLastComment` (`DateLastComment`),
  KEY `IX_Discussion_RegardingID` (`RegardingID`),
  KEY `IX_Discussion_CategoryPages` (`CategoryID`,`DateLastComment`),
  KEY `FK_Discussion_CategoryID` (`CategoryID`),
  KEY `FK_Discussion_InsertUserID` (`InsertUserID`),
  FULLTEXT KEY `TX_Discussion` (`Name`,`Body`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_Discussion`
--

LOCK TABLES `GDN_Discussion` WRITE;
/*!40000 ALTER TABLE `GDN_Discussion` DISABLE KEYS */;
INSERT INTO `GDN_Discussion` VALUES (1,NULL,'stub',1,2,NULL,NULL,1,'BAM! You&rsquo;ve got a sweet forum','There&rsquo;s nothing sweeter than a fresh new forum, ready to welcome your community. A Vanilla Forum has all the bits and pieces you need to build an awesome discussion platform customized to your needs. Here&rsquo;s a few tips:\n<ul>\n<li>Use the <a href=\"/dashboard/settings/gettingstarted\">Getting Started</a> list in the Dashboard to configure your site.</li>\n<li>Don&rsquo;t use too many categories. We recommend 3-8. Keep it simple!\n<li>&ldquo;Announce&rdquo; a discussion (click the gear) to stick to the top of the list, and &ldquo;Close&rdquo; it to stop further comments.</li>\n<li>Use &ldquo;Sink&rdquo; to take attention away from a discussion. New comments will no longer bring it back to the top of the list.</li>\n<li>Bookmark a discussion (click the star) to get notifications for new comments. You can edit notification settings from your profile.</li>\n</ul>\nGo ahead and edit or delete this discussion, then spread the word to get this place cooking. Cheers!','Html',NULL,1,NULL,5,0,0,0,'2014-09-14 01:24:43',NULL,NULL,NULL,'2014-09-14 01:24:43',2,NULL,NULL,NULL),(2,NULL,NULL,5,1,NULL,NULL,NULL,'The world is closed :(','Fix it!','Html',NULL,0,NULL,2,0,0,0,'2014-09-14 05:34:51',NULL,'192.168.23.1',NULL,'2014-09-14 05:34:51',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `GDN_Discussion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_Draft`
--

DROP TABLE IF EXISTS `GDN_Draft`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_Draft` (
  `DraftID` int(11) NOT NULL AUTO_INCREMENT,
  `DiscussionID` int(11) DEFAULT NULL,
  `CategoryID` int(11) DEFAULT NULL,
  `InsertUserID` int(11) NOT NULL,
  `UpdateUserID` int(11) NOT NULL,
  `Name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Closed` tinyint(4) NOT NULL DEFAULT '0',
  `Announce` tinyint(4) NOT NULL DEFAULT '0',
  `Sink` tinyint(4) NOT NULL DEFAULT '0',
  `Body` text COLLATE utf8_unicode_ci NOT NULL,
  `Format` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateInserted` datetime NOT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`DraftID`),
  KEY `FK_Draft_DiscussionID` (`DiscussionID`),
  KEY `FK_Draft_CategoryID` (`CategoryID`),
  KEY `FK_Draft_InsertUserID` (`InsertUserID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_Draft`
--

LOCK TABLES `GDN_Draft` WRITE;
/*!40000 ALTER TABLE `GDN_Draft` DISABLE KEYS */;
/*!40000 ALTER TABLE `GDN_Draft` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_Invitation`
--

DROP TABLE IF EXISTS `GDN_Invitation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_Invitation` (
  `InvitationID` int(11) NOT NULL AUTO_INCREMENT,
  `Email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `Code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `InsertUserID` int(11) DEFAULT NULL,
  `DateInserted` datetime NOT NULL,
  `AcceptedUserID` int(11) DEFAULT NULL,
  PRIMARY KEY (`InvitationID`),
  KEY `FK_Invitation_InsertUserID` (`InsertUserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_Invitation`
--

LOCK TABLES `GDN_Invitation` WRITE;
/*!40000 ALTER TABLE `GDN_Invitation` DISABLE KEYS */;
/*!40000 ALTER TABLE `GDN_Invitation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_Log`
--

DROP TABLE IF EXISTS `GDN_Log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_Log` (
  `LogID` int(11) NOT NULL AUTO_INCREMENT,
  `Operation` enum('Delete','Edit','Spam','Moderate','Pending','Ban','Error') COLLATE utf8_unicode_ci NOT NULL,
  `RecordType` enum('Discussion','Comment','User','Registration','Activity','ActivityComment','Configuration','Group') COLLATE utf8_unicode_ci NOT NULL,
  `TransactionLogID` int(11) DEFAULT NULL,
  `RecordID` int(11) DEFAULT NULL,
  `RecordUserID` int(11) DEFAULT NULL,
  `RecordDate` datetime NOT NULL,
  `RecordIPAddress` varchar(39) COLLATE utf8_unicode_ci DEFAULT NULL,
  `InsertUserID` int(11) NOT NULL,
  `DateInserted` datetime NOT NULL,
  `InsertIPAddress` varchar(39) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OtherUserIDs` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `ParentRecordID` int(11) DEFAULT NULL,
  `CategoryID` int(11) DEFAULT NULL,
  `Data` mediumtext COLLATE utf8_unicode_ci,
  `CountGroup` int(11) DEFAULT NULL,
  PRIMARY KEY (`LogID`),
  KEY `IX_Log_RecordType` (`RecordType`),
  KEY `IX_Log_RecordID` (`RecordID`),
  KEY `IX_Log_RecordIPAddress` (`RecordIPAddress`),
  KEY `IX_Log_ParentRecordID` (`ParentRecordID`),
  KEY `FK_Log_CategoryID` (`CategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_Log`
--

LOCK TABLES `GDN_Log` WRITE;
/*!40000 ALTER TABLE `GDN_Log` DISABLE KEYS */;
INSERT INTO `GDN_Log` VALUES (1,'Edit','Configuration',NULL,NULL,NULL,'2014-09-14 01:24:43',NULL,1,'2014-09-14 01:24:43','192.168.23.1','',NULL,NULL,NULL,'a:1:{s:4:\"_New\";a:7:{s:13:\"Conversations\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:8:\"Database\";a:4:{s:4:\"Name\";s:11:\"elevenforum\";s:4:\"Host\";s:9:\"localhost\";s:4:\"User\";s:6:\"eleven\";s:8:\"Password\";s:11:\"eleventest@\";}s:19:\"EnabledApplications\";a:2:{s:13:\"Conversations\";s:13:\"conversations\";s:7:\"Vanilla\";s:7:\"vanilla\";}s:14:\"EnabledPlugins\";a:2:{s:14:\"GettingStarted\";s:14:\"GettingStarted\";s:8:\"HtmLawed\";s:8:\"HtmLawed\";}s:6:\"Garden\";a:11:{s:5:\"Title\";s:13:\"Eleven Forums\";s:6:\"Cookie\";a:2:{s:4:\"Salt\";s:10:\"58AUCBGDRC\";s:6:\"Domain\";s:0:\"\";}s:12:\"Registration\";a:1:{s:12:\"ConfirmEmail\";b:1;}s:5:\"Email\";a:1:{s:11:\"SupportName\";s:13:\"Eleven Forums\";}s:14:\"InputFormatter\";s:4:\"Html\";s:7:\"Version\";s:5:\"2.1.3\";s:11:\"RewriteUrls\";b:0;s:4:\"Cdns\";a:1:{s:7:\"Disable\";b:0;}s:16:\"CanProcessImages\";b:0;s:12:\"SystemUserID\";s:1:\"2\";s:9:\"Installed\";b:1;}s:6:\"Routes\";a:1:{s:17:\"DefaultController\";s:11:\"discussions\";}s:7:\"Vanilla\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}}}',NULL),(2,'Edit','Configuration',NULL,NULL,NULL,'2014-09-14 01:24:52',NULL,1,'2014-09-14 01:24:52','192.168.23.1','',NULL,NULL,NULL,'a:8:{s:13:\"Conversations\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:8:\"Database\";a:4:{s:4:\"Name\";s:11:\"elevenforum\";s:4:\"Host\";s:9:\"localhost\";s:4:\"User\";s:6:\"eleven\";s:8:\"Password\";s:11:\"eleventest@\";}s:19:\"EnabledApplications\";a:2:{s:13:\"Conversations\";s:13:\"conversations\";s:7:\"Vanilla\";s:7:\"vanilla\";}s:14:\"EnabledPlugins\";a:2:{s:14:\"GettingStarted\";s:14:\"GettingStarted\";s:8:\"HtmLawed\";s:8:\"HtmLawed\";}s:6:\"Garden\";a:11:{s:5:\"Title\";s:13:\"Eleven Forums\";s:6:\"Cookie\";a:2:{s:4:\"Salt\";s:10:\"58AUCBGDRC\";s:6:\"Domain\";s:0:\"\";}s:12:\"Registration\";a:1:{s:12:\"ConfirmEmail\";b:1;}s:5:\"Email\";a:1:{s:11:\"SupportName\";s:13:\"Eleven Forums\";}s:14:\"InputFormatter\";s:4:\"Html\";s:7:\"Version\";s:5:\"2.1.3\";s:11:\"RewriteUrls\";b:0;s:4:\"Cdns\";a:1:{s:7:\"Disable\";b:0;}s:16:\"CanProcessImages\";b:0;s:12:\"SystemUserID\";s:1:\"2\";s:9:\"Installed\";b:1;}s:6:\"Routes\";a:1:{s:17:\"DefaultController\";s:11:\"discussions\";}s:7:\"Vanilla\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:4:\"_New\";a:8:{s:13:\"Conversations\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:8:\"Database\";a:4:{s:4:\"Name\";s:11:\"elevenforum\";s:4:\"Host\";s:9:\"localhost\";s:4:\"User\";s:6:\"eleven\";s:8:\"Password\";s:11:\"eleventest@\";}s:19:\"EnabledApplications\";a:2:{s:13:\"Conversations\";s:13:\"conversations\";s:7:\"Vanilla\";s:7:\"vanilla\";}s:14:\"EnabledPlugins\";a:2:{s:14:\"GettingStarted\";s:14:\"GettingStarted\";s:8:\"HtmLawed\";s:8:\"HtmLawed\";}s:6:\"Garden\";a:11:{s:5:\"Title\";s:13:\"Eleven Forums\";s:6:\"Cookie\";a:2:{s:4:\"Salt\";s:10:\"58AUCBGDRC\";s:6:\"Domain\";s:0:\"\";}s:12:\"Registration\";a:1:{s:12:\"ConfirmEmail\";b:1;}s:5:\"Email\";a:1:{s:11:\"SupportName\";s:13:\"Eleven Forums\";}s:14:\"InputFormatter\";s:4:\"Html\";s:7:\"Version\";s:5:\"2.1.3\";s:11:\"RewriteUrls\";b:0;s:4:\"Cdns\";a:1:{s:7:\"Disable\";b:0;}s:16:\"CanProcessImages\";b:0;s:12:\"SystemUserID\";s:1:\"2\";s:9:\"Installed\";b:1;}s:7:\"Plugins\";a:1:{s:14:\"GettingStarted\";a:1:{s:9:\"Dashboard\";s:1:\"1\";}}s:6:\"Routes\";a:1:{s:17:\"DefaultController\";s:11:\"discussions\";}s:7:\"Vanilla\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}}}',NULL),(3,'Edit','Configuration',NULL,NULL,NULL,'2014-09-14 01:28:17',NULL,1,'2014-09-14 01:28:17','192.168.23.1','',NULL,NULL,NULL,'a:9:{s:13:\"Conversations\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:8:\"Database\";a:4:{s:4:\"Name\";s:11:\"elevenforum\";s:4:\"Host\";s:9:\"localhost\";s:4:\"User\";s:6:\"eleven\";s:8:\"Password\";s:11:\"eleventest@\";}s:19:\"EnabledApplications\";a:2:{s:13:\"Conversations\";s:13:\"conversations\";s:7:\"Vanilla\";s:7:\"vanilla\";}s:14:\"EnabledPlugins\";a:2:{s:14:\"GettingStarted\";s:14:\"GettingStarted\";s:8:\"HtmLawed\";s:8:\"HtmLawed\";}s:6:\"Garden\";a:11:{s:5:\"Title\";s:13:\"Eleven Forums\";s:6:\"Cookie\";a:2:{s:4:\"Salt\";s:10:\"58AUCBGDRC\";s:6:\"Domain\";s:0:\"\";}s:12:\"Registration\";a:1:{s:12:\"ConfirmEmail\";b:1;}s:5:\"Email\";a:1:{s:11:\"SupportName\";s:13:\"Eleven Forums\";}s:14:\"InputFormatter\";s:4:\"Html\";s:7:\"Version\";s:5:\"2.1.3\";s:11:\"RewriteUrls\";b:0;s:4:\"Cdns\";a:1:{s:7:\"Disable\";b:0;}s:16:\"CanProcessImages\";b:0;s:12:\"SystemUserID\";s:1:\"2\";s:9:\"Installed\";b:1;}s:7:\"Plugins\";a:1:{s:14:\"GettingStarted\";a:1:{s:9:\"Dashboard\";s:1:\"1\";}}s:6:\"Routes\";a:1:{s:17:\"DefaultController\";s:11:\"discussions\";}s:7:\"Vanilla\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:4:\"_New\";a:8:{s:13:\"Conversations\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:8:\"Database\";a:4:{s:4:\"Name\";s:11:\"elevenforum\";s:4:\"Host\";s:9:\"localhost\";s:4:\"User\";s:6:\"eleven\";s:8:\"Password\";s:11:\"eleventest@\";}s:19:\"EnabledApplications\";a:2:{s:13:\"Conversations\";s:13:\"conversations\";s:7:\"Vanilla\";s:7:\"vanilla\";}s:14:\"EnabledPlugins\";a:2:{s:14:\"GettingStarted\";s:14:\"GettingStarted\";s:8:\"HtmLawed\";s:8:\"HtmLawed\";}s:6:\"Garden\";a:11:{s:5:\"Title\";s:13:\"Eleven Forums\";s:6:\"Cookie\";a:2:{s:4:\"Salt\";s:10:\"58AUCBGDRC\";s:6:\"Domain\";s:0:\"\";}s:12:\"Registration\";a:1:{s:12:\"ConfirmEmail\";b:1;}s:5:\"Email\";a:1:{s:11:\"SupportName\";s:13:\"Eleven Forums\";}s:14:\"InputFormatter\";s:4:\"Html\";s:7:\"Version\";s:5:\"2.1.3\";s:11:\"RewriteUrls\";b:0;s:4:\"Cdns\";a:1:{s:7:\"Disable\";b:0;}s:16:\"CanProcessImages\";b:0;s:12:\"SystemUserID\";s:1:\"2\";s:9:\"Installed\";b:1;}s:7:\"Plugins\";a:1:{s:14:\"GettingStarted\";a:2:{s:9:\"Dashboard\";s:1:\"1\";s:7:\"Plugins\";s:1:\"1\";}}s:6:\"Routes\";a:1:{s:17:\"DefaultController\";s:11:\"discussions\";}s:7:\"Vanilla\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}}}',NULL),(4,'Edit','Configuration',NULL,NULL,NULL,'2014-09-14 01:28:40',NULL,1,'2014-09-14 01:28:40','192.168.23.1','',NULL,NULL,NULL,'a:9:{s:13:\"Conversations\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:8:\"Database\";a:4:{s:4:\"Name\";s:11:\"elevenforum\";s:4:\"Host\";s:9:\"localhost\";s:4:\"User\";s:6:\"eleven\";s:8:\"Password\";s:11:\"eleventest@\";}s:19:\"EnabledApplications\";a:2:{s:13:\"Conversations\";s:13:\"conversations\";s:7:\"Vanilla\";s:7:\"vanilla\";}s:14:\"EnabledPlugins\";a:2:{s:14:\"GettingStarted\";s:14:\"GettingStarted\";s:8:\"HtmLawed\";s:8:\"HtmLawed\";}s:6:\"Garden\";a:11:{s:5:\"Title\";s:13:\"Eleven Forums\";s:6:\"Cookie\";a:2:{s:4:\"Salt\";s:10:\"58AUCBGDRC\";s:6:\"Domain\";s:0:\"\";}s:12:\"Registration\";a:1:{s:12:\"ConfirmEmail\";b:1;}s:5:\"Email\";a:1:{s:11:\"SupportName\";s:13:\"Eleven Forums\";}s:14:\"InputFormatter\";s:4:\"Html\";s:7:\"Version\";s:5:\"2.1.3\";s:11:\"RewriteUrls\";b:0;s:4:\"Cdns\";a:1:{s:7:\"Disable\";b:0;}s:16:\"CanProcessImages\";b:0;s:12:\"SystemUserID\";s:1:\"2\";s:9:\"Installed\";b:1;}s:7:\"Plugins\";a:1:{s:14:\"GettingStarted\";a:2:{s:9:\"Dashboard\";s:1:\"1\";s:7:\"Plugins\";s:1:\"1\";}}s:6:\"Routes\";a:1:{s:17:\"DefaultController\";s:11:\"discussions\";}s:7:\"Vanilla\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:4:\"_New\";a:8:{s:13:\"Conversations\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:8:\"Database\";a:4:{s:4:\"Name\";s:11:\"elevenforum\";s:4:\"Host\";s:9:\"localhost\";s:4:\"User\";s:6:\"eleven\";s:8:\"Password\";s:11:\"eleventest@\";}s:19:\"EnabledApplications\";a:2:{s:13:\"Conversations\";s:13:\"conversations\";s:7:\"Vanilla\";s:7:\"vanilla\";}s:14:\"EnabledPlugins\";a:3:{s:14:\"GettingStarted\";s:14:\"GettingStarted\";s:8:\"HtmLawed\";s:8:\"HtmLawed\";s:9:\"jsconnect\";b:1;}s:6:\"Garden\";a:11:{s:5:\"Title\";s:13:\"Eleven Forums\";s:6:\"Cookie\";a:2:{s:4:\"Salt\";s:10:\"58AUCBGDRC\";s:6:\"Domain\";s:0:\"\";}s:12:\"Registration\";a:1:{s:12:\"ConfirmEmail\";b:1;}s:5:\"Email\";a:1:{s:11:\"SupportName\";s:13:\"Eleven Forums\";}s:14:\"InputFormatter\";s:4:\"Html\";s:7:\"Version\";s:5:\"2.1.3\";s:11:\"RewriteUrls\";b:0;s:4:\"Cdns\";a:1:{s:7:\"Disable\";b:0;}s:16:\"CanProcessImages\";b:0;s:12:\"SystemUserID\";s:1:\"2\";s:9:\"Installed\";b:1;}s:7:\"Plugins\";a:1:{s:14:\"GettingStarted\";a:2:{s:9:\"Dashboard\";s:1:\"1\";s:7:\"Plugins\";s:1:\"1\";}}s:6:\"Routes\";a:1:{s:17:\"DefaultController\";s:11:\"discussions\";}s:7:\"Vanilla\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}}}',NULL),(5,'Edit','Configuration',NULL,NULL,NULL,'2014-09-14 01:49:14',NULL,1,'2014-09-14 01:49:14','192.168.23.1','',NULL,NULL,NULL,'a:9:{s:13:\"Conversations\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:8:\"Database\";a:4:{s:4:\"Name\";s:11:\"elevenforum\";s:4:\"Host\";s:9:\"localhost\";s:4:\"User\";s:6:\"eleven\";s:8:\"Password\";s:11:\"eleventest@\";}s:19:\"EnabledApplications\";a:2:{s:13:\"Conversations\";s:13:\"conversations\";s:7:\"Vanilla\";s:7:\"vanilla\";}s:14:\"EnabledPlugins\";a:3:{s:14:\"GettingStarted\";s:14:\"GettingStarted\";s:8:\"HtmLawed\";s:8:\"HtmLawed\";s:9:\"jsconnect\";b:1;}s:6:\"Garden\";a:11:{s:5:\"Title\";s:13:\"Eleven Forums\";s:6:\"Cookie\";a:2:{s:4:\"Salt\";s:10:\"58AUCBGDRC\";s:6:\"Domain\";s:0:\"\";}s:12:\"Registration\";a:1:{s:12:\"ConfirmEmail\";b:1;}s:5:\"Email\";a:1:{s:11:\"SupportName\";s:13:\"Eleven Forums\";}s:14:\"InputFormatter\";s:4:\"Html\";s:7:\"Version\";s:5:\"2.1.3\";s:11:\"RewriteUrls\";b:0;s:4:\"Cdns\";a:1:{s:7:\"Disable\";b:0;}s:16:\"CanProcessImages\";b:0;s:12:\"SystemUserID\";s:1:\"2\";s:9:\"Installed\";b:1;}s:7:\"Plugins\";a:1:{s:14:\"GettingStarted\";a:2:{s:9:\"Dashboard\";s:1:\"1\";s:7:\"Plugins\";s:1:\"1\";}}s:6:\"Routes\";a:1:{s:17:\"DefaultController\";s:11:\"discussions\";}s:7:\"Vanilla\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:4:\"_New\";a:8:{s:13:\"Conversations\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:8:\"Database\";a:4:{s:4:\"Name\";s:11:\"elevenforum\";s:4:\"Host\";s:9:\"localhost\";s:4:\"User\";s:6:\"eleven\";s:8:\"Password\";s:11:\"eleventest@\";}s:19:\"EnabledApplications\";a:2:{s:13:\"Conversations\";s:13:\"conversations\";s:7:\"Vanilla\";s:7:\"vanilla\";}s:14:\"EnabledPlugins\";a:3:{s:14:\"GettingStarted\";s:14:\"GettingStarted\";s:8:\"HtmLawed\";s:8:\"HtmLawed\";s:9:\"jsconnect\";b:1;}s:6:\"Garden\";a:11:{s:5:\"Title\";s:13:\"Eleven Forums\";s:6:\"Cookie\";a:2:{s:4:\"Salt\";s:10:\"58AUCBGDRC\";s:6:\"Domain\";s:0:\"\";}s:12:\"Registration\";a:7:{s:12:\"ConfirmEmail\";b:0;s:6:\"Method\";s:7:\"Connect\";s:17:\"CaptchaPrivateKey\";s:0:\"\";s:16:\"CaptchaPublicKey\";s:0:\"\";s:16:\"InviteExpiration\";s:7:\"-1 week\";s:16:\"ConfirmEmailRole\";s:1:\"3\";s:11:\"InviteRoles\";a:5:{i:3;s:1:\"0\";i:4;s:1:\"0\";i:8;s:1:\"0\";i:16;s:1:\"0\";i:32;s:1:\"0\";}}s:5:\"Email\";a:1:{s:11:\"SupportName\";s:13:\"Eleven Forums\";}s:14:\"InputFormatter\";s:4:\"Html\";s:7:\"Version\";s:5:\"2.1.3\";s:11:\"RewriteUrls\";b:0;s:4:\"Cdns\";a:1:{s:7:\"Disable\";b:0;}s:16:\"CanProcessImages\";b:0;s:12:\"SystemUserID\";s:1:\"2\";s:9:\"Installed\";b:1;}s:7:\"Plugins\";a:1:{s:14:\"GettingStarted\";a:3:{s:9:\"Dashboard\";s:1:\"1\";s:7:\"Plugins\";s:1:\"1\";s:12:\"Registration\";s:1:\"1\";}}s:6:\"Routes\";a:1:{s:17:\"DefaultController\";s:11:\"discussions\";}s:7:\"Vanilla\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}}}',NULL),(6,'Edit','Configuration',NULL,NULL,NULL,'2014-09-14 05:22:45',NULL,1,'2014-09-14 05:22:45','192.168.23.1','',NULL,NULL,NULL,'a:9:{s:13:\"Conversations\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:8:\"Database\";a:4:{s:4:\"Name\";s:11:\"elevenforum\";s:4:\"Host\";s:9:\"localhost\";s:4:\"User\";s:6:\"eleven\";s:8:\"Password\";s:11:\"eleventest@\";}s:19:\"EnabledApplications\";a:2:{s:13:\"Conversations\";s:13:\"conversations\";s:7:\"Vanilla\";s:7:\"vanilla\";}s:14:\"EnabledPlugins\";a:3:{s:14:\"GettingStarted\";s:14:\"GettingStarted\";s:8:\"HtmLawed\";s:8:\"HtmLawed\";s:9:\"jsconnect\";b:1;}s:6:\"Garden\";a:11:{s:5:\"Title\";s:13:\"Eleven Forums\";s:6:\"Cookie\";a:2:{s:4:\"Salt\";s:10:\"58AUCBGDRC\";s:6:\"Domain\";s:0:\"\";}s:12:\"Registration\";a:7:{s:12:\"ConfirmEmail\";b:0;s:6:\"Method\";s:7:\"Connect\";s:17:\"CaptchaPrivateKey\";s:0:\"\";s:16:\"CaptchaPublicKey\";s:0:\"\";s:16:\"InviteExpiration\";s:7:\"-1 week\";s:16:\"ConfirmEmailRole\";s:1:\"3\";s:11:\"InviteRoles\";a:5:{i:3;s:1:\"0\";i:4;s:1:\"0\";i:8;s:1:\"0\";i:16;s:1:\"0\";i:32;s:1:\"0\";}}s:5:\"Email\";a:1:{s:11:\"SupportName\";s:13:\"Eleven Forums\";}s:14:\"InputFormatter\";s:4:\"Html\";s:7:\"Version\";s:5:\"2.1.3\";s:11:\"RewriteUrls\";b:0;s:4:\"Cdns\";a:1:{s:7:\"Disable\";b:0;}s:16:\"CanProcessImages\";b:0;s:12:\"SystemUserID\";s:1:\"2\";s:9:\"Installed\";b:1;}s:7:\"Plugins\";a:1:{s:14:\"GettingStarted\";a:3:{s:9:\"Dashboard\";s:1:\"1\";s:7:\"Plugins\";s:1:\"1\";s:12:\"Registration\";s:1:\"1\";}}s:6:\"Routes\";a:1:{s:17:\"DefaultController\";s:11:\"discussions\";}s:7:\"Vanilla\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:4:\"_New\";a:8:{s:13:\"Conversations\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:8:\"Database\";a:4:{s:4:\"Name\";s:11:\"elevenforum\";s:4:\"Host\";s:9:\"localhost\";s:4:\"User\";s:6:\"eleven\";s:8:\"Password\";s:11:\"eleventest@\";}s:19:\"EnabledApplications\";a:2:{s:13:\"Conversations\";s:13:\"conversations\";s:7:\"Vanilla\";s:7:\"vanilla\";}s:14:\"EnabledPlugins\";a:3:{s:14:\"GettingStarted\";s:14:\"GettingStarted\";s:8:\"HtmLawed\";s:8:\"HtmLawed\";s:9:\"jsconnect\";b:1;}s:6:\"Garden\";a:11:{s:5:\"Title\";s:13:\"Eleven Forums\";s:6:\"Cookie\";a:2:{s:4:\"Salt\";s:10:\"58AUCBGDRC\";s:6:\"Domain\";s:0:\"\";}s:12:\"Registration\";a:7:{s:12:\"ConfirmEmail\";b:0;s:6:\"Method\";s:7:\"Connect\";s:17:\"CaptchaPrivateKey\";s:0:\"\";s:16:\"CaptchaPublicKey\";s:0:\"\";s:16:\"InviteExpiration\";s:7:\"-1 week\";s:16:\"ConfirmEmailRole\";s:1:\"3\";s:11:\"InviteRoles\";a:5:{i:3;s:1:\"0\";i:4;s:1:\"0\";i:8;s:1:\"0\";i:16;s:1:\"0\";i:32;s:1:\"0\";}}s:5:\"Email\";a:1:{s:11:\"SupportName\";s:13:\"Eleven Forums\";}s:14:\"InputFormatter\";s:4:\"Html\";s:7:\"Version\";s:5:\"2.1.3\";s:11:\"RewriteUrls\";b:0;s:4:\"Cdns\";a:1:{s:7:\"Disable\";b:0;}s:16:\"CanProcessImages\";b:0;s:12:\"SystemUserID\";s:1:\"2\";s:9:\"Installed\";b:1;}s:7:\"Plugins\";a:1:{s:14:\"GettingStarted\";a:3:{s:9:\"Dashboard\";s:1:\"1\";s:7:\"Plugins\";s:1:\"1\";s:12:\"Registration\";s:1:\"1\";}}s:6:\"Routes\";a:1:{s:17:\"DefaultController\";a:2:{i:0;s:10:\"categories\";i:1;s:8:\"Internal\";}}s:7:\"Vanilla\";a:3:{s:7:\"Version\";s:5:\"2.1.3\";s:11:\"Discussions\";a:1:{s:6:\"Layout\";s:6:\"modern\";}s:10:\"Categories\";a:1:{s:6:\"Layout\";s:5:\"mixed\";}}}}',NULL),(7,'Edit','Configuration',NULL,NULL,NULL,'2014-09-14 05:22:58',NULL,1,'2014-09-14 05:22:58','192.168.23.1','',NULL,NULL,NULL,'a:9:{s:13:\"Conversations\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:8:\"Database\";a:4:{s:4:\"Name\";s:11:\"elevenforum\";s:4:\"Host\";s:9:\"localhost\";s:4:\"User\";s:6:\"eleven\";s:8:\"Password\";s:11:\"eleventest@\";}s:19:\"EnabledApplications\";a:2:{s:13:\"Conversations\";s:13:\"conversations\";s:7:\"Vanilla\";s:7:\"vanilla\";}s:14:\"EnabledPlugins\";a:3:{s:14:\"GettingStarted\";s:14:\"GettingStarted\";s:8:\"HtmLawed\";s:8:\"HtmLawed\";s:9:\"jsconnect\";b:1;}s:6:\"Garden\";a:11:{s:5:\"Title\";s:13:\"Eleven Forums\";s:6:\"Cookie\";a:2:{s:4:\"Salt\";s:10:\"58AUCBGDRC\";s:6:\"Domain\";s:0:\"\";}s:12:\"Registration\";a:7:{s:12:\"ConfirmEmail\";b:0;s:6:\"Method\";s:7:\"Connect\";s:17:\"CaptchaPrivateKey\";s:0:\"\";s:16:\"CaptchaPublicKey\";s:0:\"\";s:16:\"InviteExpiration\";s:7:\"-1 week\";s:16:\"ConfirmEmailRole\";s:1:\"3\";s:11:\"InviteRoles\";a:5:{i:3;s:1:\"0\";i:4;s:1:\"0\";i:8;s:1:\"0\";i:16;s:1:\"0\";i:32;s:1:\"0\";}}s:5:\"Email\";a:1:{s:11:\"SupportName\";s:13:\"Eleven Forums\";}s:14:\"InputFormatter\";s:4:\"Html\";s:7:\"Version\";s:5:\"2.1.3\";s:11:\"RewriteUrls\";b:0;s:4:\"Cdns\";a:1:{s:7:\"Disable\";b:0;}s:16:\"CanProcessImages\";b:0;s:12:\"SystemUserID\";s:1:\"2\";s:9:\"Installed\";b:1;}s:7:\"Plugins\";a:1:{s:14:\"GettingStarted\";a:3:{s:9:\"Dashboard\";s:1:\"1\";s:7:\"Plugins\";s:1:\"1\";s:12:\"Registration\";s:1:\"1\";}}s:6:\"Routes\";a:1:{s:17:\"DefaultController\";a:2:{i:0;s:10:\"categories\";i:1;s:8:\"Internal\";}}s:7:\"Vanilla\";a:3:{s:7:\"Version\";s:5:\"2.1.3\";s:11:\"Discussions\";a:1:{s:6:\"Layout\";s:6:\"modern\";}s:10:\"Categories\";a:1:{s:6:\"Layout\";s:5:\"mixed\";}}s:4:\"_New\";a:8:{s:13:\"Conversations\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:8:\"Database\";a:4:{s:4:\"Name\";s:11:\"elevenforum\";s:4:\"Host\";s:9:\"localhost\";s:4:\"User\";s:6:\"eleven\";s:8:\"Password\";s:11:\"eleventest@\";}s:19:\"EnabledApplications\";a:2:{s:13:\"Conversations\";s:13:\"conversations\";s:7:\"Vanilla\";s:7:\"vanilla\";}s:14:\"EnabledPlugins\";a:3:{s:14:\"GettingStarted\";s:14:\"GettingStarted\";s:8:\"HtmLawed\";s:8:\"HtmLawed\";s:9:\"jsconnect\";b:1;}s:6:\"Garden\";a:11:{s:5:\"Title\";s:13:\"Eleven Forums\";s:6:\"Cookie\";a:2:{s:4:\"Salt\";s:10:\"58AUCBGDRC\";s:6:\"Domain\";s:0:\"\";}s:12:\"Registration\";a:7:{s:12:\"ConfirmEmail\";b:0;s:6:\"Method\";s:7:\"Connect\";s:17:\"CaptchaPrivateKey\";s:0:\"\";s:16:\"CaptchaPublicKey\";s:0:\"\";s:16:\"InviteExpiration\";s:7:\"-1 week\";s:16:\"ConfirmEmailRole\";s:1:\"3\";s:11:\"InviteRoles\";a:5:{i:3;s:1:\"0\";i:4;s:1:\"0\";i:8;s:1:\"0\";i:16;s:1:\"0\";i:32;s:1:\"0\";}}s:5:\"Email\";a:1:{s:11:\"SupportName\";s:13:\"Eleven Forums\";}s:14:\"InputFormatter\";s:4:\"Html\";s:7:\"Version\";s:5:\"2.1.3\";s:11:\"RewriteUrls\";b:0;s:4:\"Cdns\";a:1:{s:7:\"Disable\";b:0;}s:16:\"CanProcessImages\";b:0;s:12:\"SystemUserID\";s:1:\"2\";s:9:\"Installed\";b:1;}s:7:\"Plugins\";a:1:{s:14:\"GettingStarted\";a:4:{s:9:\"Dashboard\";s:1:\"1\";s:7:\"Plugins\";s:1:\"1\";s:12:\"Registration\";s:1:\"1\";s:10:\"Categories\";s:1:\"1\";}}s:6:\"Routes\";a:1:{s:17:\"DefaultController\";a:2:{i:0;s:10:\"categories\";i:1;s:8:\"Internal\";}}s:7:\"Vanilla\";a:3:{s:7:\"Version\";s:5:\"2.1.3\";s:11:\"Discussions\";a:1:{s:6:\"Layout\";s:6:\"modern\";}s:10:\"Categories\";a:1:{s:6:\"Layout\";s:5:\"mixed\";}}}}',NULL),(8,'Edit','Configuration',NULL,NULL,NULL,'2014-09-14 05:34:05',NULL,1,'2014-09-14 05:34:05','192.168.23.1','',NULL,NULL,NULL,'a:9:{s:13:\"Conversations\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:8:\"Database\";a:4:{s:4:\"Name\";s:11:\"elevenforum\";s:4:\"Host\";s:9:\"localhost\";s:4:\"User\";s:6:\"eleven\";s:8:\"Password\";s:11:\"eleventest@\";}s:19:\"EnabledApplications\";a:2:{s:13:\"Conversations\";s:13:\"conversations\";s:7:\"Vanilla\";s:7:\"vanilla\";}s:14:\"EnabledPlugins\";a:3:{s:14:\"GettingStarted\";s:14:\"GettingStarted\";s:8:\"HtmLawed\";s:8:\"HtmLawed\";s:9:\"jsconnect\";b:1;}s:6:\"Garden\";a:11:{s:5:\"Title\";s:13:\"Eleven Forums\";s:6:\"Cookie\";a:2:{s:4:\"Salt\";s:10:\"58AUCBGDRC\";s:6:\"Domain\";s:0:\"\";}s:12:\"Registration\";a:7:{s:12:\"ConfirmEmail\";b:0;s:6:\"Method\";s:7:\"Connect\";s:17:\"CaptchaPrivateKey\";s:0:\"\";s:16:\"CaptchaPublicKey\";s:0:\"\";s:16:\"InviteExpiration\";s:7:\"-1 week\";s:16:\"ConfirmEmailRole\";s:1:\"3\";s:11:\"InviteRoles\";a:5:{i:3;s:1:\"0\";i:4;s:1:\"0\";i:8;s:1:\"0\";i:16;s:1:\"0\";i:32;s:1:\"0\";}}s:5:\"Email\";a:1:{s:11:\"SupportName\";s:13:\"Eleven Forums\";}s:14:\"InputFormatter\";s:4:\"Html\";s:7:\"Version\";s:5:\"2.1.3\";s:11:\"RewriteUrls\";b:0;s:4:\"Cdns\";a:1:{s:7:\"Disable\";b:0;}s:16:\"CanProcessImages\";b:0;s:12:\"SystemUserID\";s:1:\"2\";s:9:\"Installed\";b:1;}s:7:\"Plugins\";a:1:{s:14:\"GettingStarted\";a:4:{s:9:\"Dashboard\";s:1:\"1\";s:7:\"Plugins\";s:1:\"1\";s:12:\"Registration\";s:1:\"1\";s:10:\"Categories\";s:1:\"1\";}}s:6:\"Routes\";a:1:{s:17:\"DefaultController\";a:2:{i:0;s:10:\"categories\";i:1;s:8:\"Internal\";}}s:7:\"Vanilla\";a:3:{s:7:\"Version\";s:5:\"2.1.3\";s:11:\"Discussions\";a:1:{s:6:\"Layout\";s:6:\"modern\";}s:10:\"Categories\";a:1:{s:6:\"Layout\";s:5:\"mixed\";}}s:4:\"_New\";a:8:{s:13:\"Conversations\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:8:\"Database\";a:4:{s:4:\"Name\";s:11:\"elevenforum\";s:4:\"Host\";s:9:\"localhost\";s:4:\"User\";s:6:\"eleven\";s:8:\"Password\";s:11:\"eleventest@\";}s:19:\"EnabledApplications\";a:2:{s:13:\"Conversations\";s:13:\"conversations\";s:7:\"Vanilla\";s:7:\"vanilla\";}s:14:\"EnabledPlugins\";a:3:{s:14:\"GettingStarted\";s:14:\"GettingStarted\";s:8:\"HtmLawed\";s:8:\"HtmLawed\";s:9:\"jsconnect\";b:1;}s:6:\"Garden\";a:11:{s:5:\"Title\";s:13:\"Eleven Forums\";s:6:\"Cookie\";a:2:{s:4:\"Salt\";s:10:\"58AUCBGDRC\";s:6:\"Domain\";s:0:\"\";}s:12:\"Registration\";a:7:{s:12:\"ConfirmEmail\";b:0;s:6:\"Method\";s:7:\"Connect\";s:17:\"CaptchaPrivateKey\";s:0:\"\";s:16:\"CaptchaPublicKey\";s:0:\"\";s:16:\"InviteExpiration\";s:7:\"-1 week\";s:16:\"ConfirmEmailRole\";s:1:\"3\";s:11:\"InviteRoles\";a:5:{i:3;s:1:\"0\";i:4;s:1:\"0\";i:8;s:1:\"0\";i:16;s:1:\"0\";i:32;s:1:\"0\";}}s:5:\"Email\";a:1:{s:11:\"SupportName\";s:13:\"Eleven Forums\";}s:14:\"InputFormatter\";s:4:\"Html\";s:7:\"Version\";s:5:\"2.1.3\";s:11:\"RewriteUrls\";b:0;s:4:\"Cdns\";a:1:{s:7:\"Disable\";b:0;}s:16:\"CanProcessImages\";b:0;s:12:\"SystemUserID\";s:1:\"2\";s:9:\"Installed\";b:1;}s:7:\"Plugins\";a:1:{s:14:\"GettingStarted\";a:4:{s:9:\"Dashboard\";s:1:\"1\";s:7:\"Plugins\";s:1:\"1\";s:12:\"Registration\";s:1:\"1\";s:10:\"Categories\";s:1:\"1\";}}s:6:\"Routes\";a:1:{s:17:\"DefaultController\";a:2:{i:0;s:10:\"categories\";i:1;s:8:\"Internal\";}}s:7:\"Vanilla\";a:3:{s:7:\"Version\";s:5:\"2.1.3\";s:11:\"Discussions\";a:1:{s:6:\"Layout\";s:6:\"modern\";}s:10:\"Categories\";a:4:{s:6:\"Layout\";s:5:\"mixed\";s:15:\"MaxDisplayDepth\";s:1:\"3\";s:10:\"DoHeadings\";b:0;s:10:\"HideModule\";b:0;}}}}',NULL),(9,'Edit','Configuration',NULL,NULL,NULL,'2014-09-14 05:34:46',NULL,1,'2014-09-14 05:34:46','192.168.23.1','',NULL,NULL,NULL,'a:9:{s:13:\"Conversations\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:8:\"Database\";a:4:{s:4:\"Name\";s:11:\"elevenforum\";s:4:\"Host\";s:9:\"localhost\";s:4:\"User\";s:6:\"eleven\";s:8:\"Password\";s:11:\"eleventest@\";}s:19:\"EnabledApplications\";a:2:{s:13:\"Conversations\";s:13:\"conversations\";s:7:\"Vanilla\";s:7:\"vanilla\";}s:14:\"EnabledPlugins\";a:3:{s:14:\"GettingStarted\";s:14:\"GettingStarted\";s:8:\"HtmLawed\";s:8:\"HtmLawed\";s:9:\"jsconnect\";b:1;}s:6:\"Garden\";a:11:{s:5:\"Title\";s:13:\"Eleven Forums\";s:6:\"Cookie\";a:2:{s:4:\"Salt\";s:10:\"58AUCBGDRC\";s:6:\"Domain\";s:0:\"\";}s:12:\"Registration\";a:7:{s:12:\"ConfirmEmail\";b:0;s:6:\"Method\";s:7:\"Connect\";s:17:\"CaptchaPrivateKey\";s:0:\"\";s:16:\"CaptchaPublicKey\";s:0:\"\";s:16:\"InviteExpiration\";s:7:\"-1 week\";s:16:\"ConfirmEmailRole\";s:1:\"3\";s:11:\"InviteRoles\";a:5:{i:3;s:1:\"0\";i:4;s:1:\"0\";i:8;s:1:\"0\";i:16;s:1:\"0\";i:32;s:1:\"0\";}}s:5:\"Email\";a:1:{s:11:\"SupportName\";s:13:\"Eleven Forums\";}s:14:\"InputFormatter\";s:4:\"Html\";s:7:\"Version\";s:5:\"2.1.3\";s:11:\"RewriteUrls\";b:0;s:4:\"Cdns\";a:1:{s:7:\"Disable\";b:0;}s:16:\"CanProcessImages\";b:0;s:12:\"SystemUserID\";s:1:\"2\";s:9:\"Installed\";b:1;}s:7:\"Plugins\";a:1:{s:14:\"GettingStarted\";a:4:{s:9:\"Dashboard\";s:1:\"1\";s:7:\"Plugins\";s:1:\"1\";s:12:\"Registration\";s:1:\"1\";s:10:\"Categories\";s:1:\"1\";}}s:6:\"Routes\";a:1:{s:17:\"DefaultController\";a:2:{i:0;s:10:\"categories\";i:1;s:8:\"Internal\";}}s:7:\"Vanilla\";a:3:{s:7:\"Version\";s:5:\"2.1.3\";s:11:\"Discussions\";a:1:{s:6:\"Layout\";s:6:\"modern\";}s:10:\"Categories\";a:4:{s:6:\"Layout\";s:5:\"mixed\";s:15:\"MaxDisplayDepth\";s:1:\"3\";s:10:\"DoHeadings\";b:0;s:10:\"HideModule\";b:0;}}s:4:\"_New\";a:8:{s:13:\"Conversations\";a:1:{s:7:\"Version\";s:5:\"2.1.3\";}s:8:\"Database\";a:4:{s:4:\"Name\";s:11:\"elevenforum\";s:4:\"Host\";s:9:\"localhost\";s:4:\"User\";s:6:\"eleven\";s:8:\"Password\";s:11:\"eleventest@\";}s:19:\"EnabledApplications\";a:2:{s:13:\"Conversations\";s:13:\"conversations\";s:7:\"Vanilla\";s:7:\"vanilla\";}s:14:\"EnabledPlugins\";a:3:{s:14:\"GettingStarted\";s:14:\"GettingStarted\";s:8:\"HtmLawed\";s:8:\"HtmLawed\";s:9:\"jsconnect\";b:1;}s:6:\"Garden\";a:11:{s:5:\"Title\";s:13:\"Eleven Forums\";s:6:\"Cookie\";a:2:{s:4:\"Salt\";s:10:\"58AUCBGDRC\";s:6:\"Domain\";s:0:\"\";}s:12:\"Registration\";a:7:{s:12:\"ConfirmEmail\";b:0;s:6:\"Method\";s:7:\"Connect\";s:17:\"CaptchaPrivateKey\";s:0:\"\";s:16:\"CaptchaPublicKey\";s:0:\"\";s:16:\"InviteExpiration\";s:7:\"-1 week\";s:16:\"ConfirmEmailRole\";s:1:\"3\";s:11:\"InviteRoles\";a:5:{i:3;s:1:\"0\";i:4;s:1:\"0\";i:8;s:1:\"0\";i:16;s:1:\"0\";i:32;s:1:\"0\";}}s:5:\"Email\";a:1:{s:11:\"SupportName\";s:13:\"Eleven Forums\";}s:14:\"InputFormatter\";s:4:\"Html\";s:7:\"Version\";s:5:\"2.1.3\";s:11:\"RewriteUrls\";b:0;s:4:\"Cdns\";a:1:{s:7:\"Disable\";b:0;}s:16:\"CanProcessImages\";b:0;s:12:\"SystemUserID\";s:1:\"2\";s:9:\"Installed\";b:1;}s:7:\"Plugins\";a:1:{s:14:\"GettingStarted\";a:5:{s:9:\"Dashboard\";s:1:\"1\";s:7:\"Plugins\";s:1:\"1\";s:12:\"Registration\";s:1:\"1\";s:10:\"Categories\";s:1:\"1\";s:10:\"Discussion\";s:1:\"1\";}}s:6:\"Routes\";a:1:{s:17:\"DefaultController\";a:2:{i:0;s:10:\"categories\";i:1;s:8:\"Internal\";}}s:7:\"Vanilla\";a:3:{s:7:\"Version\";s:5:\"2.1.3\";s:11:\"Discussions\";a:1:{s:6:\"Layout\";s:6:\"modern\";}s:10:\"Categories\";a:4:{s:6:\"Layout\";s:5:\"mixed\";s:15:\"MaxDisplayDepth\";s:1:\"3\";s:10:\"DoHeadings\";b:0;s:10:\"HideModule\";b:0;}}}}',NULL);
/*!40000 ALTER TABLE `GDN_Log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_Media`
--

DROP TABLE IF EXISTS `GDN_Media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_Media` (
  `MediaID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Type` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `Size` int(11) NOT NULL,
  `InsertUserID` int(11) NOT NULL,
  `DateInserted` datetime NOT NULL,
  `ForeignID` int(11) DEFAULT NULL,
  `ForeignTable` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ImageWidth` smallint(5) unsigned DEFAULT NULL,
  `ImageHeight` smallint(5) unsigned DEFAULT NULL,
  `ThumbWidth` smallint(5) unsigned DEFAULT NULL,
  `ThumbHeight` smallint(5) unsigned DEFAULT NULL,
  `ThumbPath` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`MediaID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_Media`
--

LOCK TABLES `GDN_Media` WRITE;
/*!40000 ALTER TABLE `GDN_Media` DISABLE KEYS */;
/*!40000 ALTER TABLE `GDN_Media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_Message`
--

DROP TABLE IF EXISTS `GDN_Message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_Message` (
  `MessageID` int(11) NOT NULL AUTO_INCREMENT,
  `Content` text COLLATE utf8_unicode_ci NOT NULL,
  `Format` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AllowDismiss` tinyint(4) NOT NULL DEFAULT '1',
  `Enabled` tinyint(4) NOT NULL DEFAULT '1',
  `Application` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Controller` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AssetTarget` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CssClass` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`MessageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_Message`
--

LOCK TABLES `GDN_Message` WRITE;
/*!40000 ALTER TABLE `GDN_Message` DISABLE KEYS */;
/*!40000 ALTER TABLE `GDN_Message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_Permission`
--

DROP TABLE IF EXISTS `GDN_Permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_Permission` (
  `PermissionID` int(11) NOT NULL AUTO_INCREMENT,
  `RoleID` int(11) NOT NULL DEFAULT '0',
  `JunctionTable` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `JunctionColumn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `JunctionID` int(11) DEFAULT NULL,
  `Garden.Email.View` tinyint(4) NOT NULL DEFAULT '0',
  `Garden.Settings.Manage` tinyint(4) NOT NULL DEFAULT '0',
  `Garden.Settings.View` tinyint(4) NOT NULL DEFAULT '0',
  `Garden.Messages.Manage` tinyint(4) NOT NULL DEFAULT '0',
  `Garden.SignIn.Allow` tinyint(4) NOT NULL DEFAULT '0',
  `Garden.Users.Add` tinyint(4) NOT NULL DEFAULT '0',
  `Garden.Users.Edit` tinyint(4) NOT NULL DEFAULT '0',
  `Garden.Users.Delete` tinyint(4) NOT NULL DEFAULT '0',
  `Garden.Users.Approve` tinyint(4) NOT NULL DEFAULT '0',
  `Garden.Activity.Delete` tinyint(4) NOT NULL DEFAULT '0',
  `Garden.Activity.View` tinyint(4) NOT NULL DEFAULT '0',
  `Garden.Profiles.View` tinyint(4) NOT NULL DEFAULT '0',
  `Garden.Profiles.Edit` tinyint(4) NOT NULL DEFAULT '0',
  `Garden.Moderation.Manage` tinyint(4) NOT NULL DEFAULT '0',
  `Garden.Curation.Manage` tinyint(4) NOT NULL DEFAULT '0',
  `Garden.PersonalInfo.View` tinyint(4) NOT NULL DEFAULT '0',
  `Garden.AdvancedNotifications.Allow` tinyint(4) NOT NULL DEFAULT '0',
  `Conversations.Moderation.Manage` tinyint(4) NOT NULL DEFAULT '0',
  `Conversations.Conversations.Add` tinyint(4) NOT NULL DEFAULT '0',
  `Vanilla.Approval.Require` tinyint(4) NOT NULL DEFAULT '0',
  `Vanilla.Comments.Me` tinyint(4) NOT NULL DEFAULT '0',
  `Vanilla.Discussions.View` tinyint(4) NOT NULL DEFAULT '0',
  `Vanilla.Discussions.Add` tinyint(4) NOT NULL DEFAULT '0',
  `Vanilla.Discussions.Edit` tinyint(4) NOT NULL DEFAULT '0',
  `Vanilla.Discussions.Announce` tinyint(4) NOT NULL DEFAULT '0',
  `Vanilla.Discussions.Sink` tinyint(4) NOT NULL DEFAULT '0',
  `Vanilla.Discussions.Close` tinyint(4) NOT NULL DEFAULT '0',
  `Vanilla.Discussions.Delete` tinyint(4) NOT NULL DEFAULT '0',
  `Vanilla.Comments.Add` tinyint(4) NOT NULL DEFAULT '0',
  `Vanilla.Comments.Edit` tinyint(4) NOT NULL DEFAULT '0',
  `Vanilla.Comments.Delete` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`PermissionID`),
  KEY `FK_Permission_RoleID` (`RoleID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_Permission`
--

LOCK TABLES `GDN_Permission` WRITE;
/*!40000 ALTER TABLE `GDN_Permission` DISABLE KEYS */;
INSERT INTO `GDN_Permission` VALUES (1,0,NULL,NULL,NULL,3,2,2,2,3,2,2,2,2,2,3,3,3,2,2,2,2,2,3,2,3,0,0,0,0,0,0,0,0,0,0),(2,2,NULL,NULL,NULL,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0),(3,3,NULL,NULL,NULL,1,0,0,0,1,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0),(4,4,NULL,NULL,NULL,1,0,0,0,1,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0),(5,8,NULL,NULL,NULL,1,0,0,0,1,0,0,0,0,0,1,1,1,0,0,0,0,0,1,0,1,1,1,0,0,0,0,0,1,0,0),(6,32,NULL,NULL,NULL,1,0,0,0,1,0,0,0,0,0,1,1,1,1,1,0,0,0,1,0,1,1,1,1,1,1,1,1,1,1,1),(7,16,NULL,NULL,NULL,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1),(8,0,'Category','PermissionCategoryID',NULL,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,2,2,2,2,2,3,2,2),(9,2,'Category','PermissionCategoryID',-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0),(10,4,'Category','PermissionCategoryID',-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0),(11,8,'Category','PermissionCategoryID',-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,1,0,0),(12,32,'Category','PermissionCategoryID',-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1),(13,16,'Category','PermissionCategoryID',-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1);
/*!40000 ALTER TABLE `GDN_Permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_Regarding`
--

DROP TABLE IF EXISTS `GDN_Regarding`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_Regarding` (
  `RegardingID` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `InsertUserID` int(11) NOT NULL,
  `DateInserted` datetime NOT NULL,
  `ForeignType` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ForeignID` int(11) NOT NULL,
  `OriginalContent` text COLLATE utf8_unicode_ci,
  `ParentType` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ParentID` int(11) DEFAULT NULL,
  `ForeignURL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Comment` text COLLATE utf8_unicode_ci NOT NULL,
  `Reports` int(11) DEFAULT NULL,
  PRIMARY KEY (`RegardingID`),
  KEY `FK_Regarding_Type` (`Type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_Regarding`
--

LOCK TABLES `GDN_Regarding` WRITE;
/*!40000 ALTER TABLE `GDN_Regarding` DISABLE KEYS */;
/*!40000 ALTER TABLE `GDN_Regarding` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_Role`
--

DROP TABLE IF EXISTS `GDN_Role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_Role` (
  `RoleID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Sort` int(11) DEFAULT NULL,
  `Deletable` tinyint(4) NOT NULL DEFAULT '1',
  `CanSession` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_Role`
--

LOCK TABLES `GDN_Role` WRITE;
/*!40000 ALTER TABLE `GDN_Role` DISABLE KEYS */;
INSERT INTO `GDN_Role` VALUES (2,'Guest','Guests can only view content. Anyone browsing the site who is not signed in is considered to be a \"Guest\".',1,0,0),(3,'Unconfirmed','Users must confirm their emails before becoming full members. They get assigned to this role.',2,1,1),(4,'Applicant','Users who have applied for membership, but have not yet been accepted. They have the same permissions as guests.',3,0,1),(8,'Member','Members can participate in discussions.',4,1,1),(16,'Administrator','Administrators have permission to do anything.',6,1,1),(32,'Moderator','Moderators have permission to edit most content.',5,1,1);
/*!40000 ALTER TABLE `GDN_Role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_Session`
--

DROP TABLE IF EXISTS `GDN_Session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_Session` (
  `SessionID` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `UserID` int(11) NOT NULL DEFAULT '0',
  `DateInserted` datetime NOT NULL,
  `DateUpdated` datetime NOT NULL,
  `TransientKey` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `Attributes` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`SessionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_Session`
--

LOCK TABLES `GDN_Session` WRITE;
/*!40000 ALTER TABLE `GDN_Session` DISABLE KEYS */;
/*!40000 ALTER TABLE `GDN_Session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_Spammer`
--

DROP TABLE IF EXISTS `GDN_Spammer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_Spammer` (
  `UserID` int(11) NOT NULL,
  `CountSpam` smallint(5) unsigned NOT NULL DEFAULT '0',
  `CountDeletedSpam` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_Spammer`
--

LOCK TABLES `GDN_Spammer` WRITE;
/*!40000 ALTER TABLE `GDN_Spammer` DISABLE KEYS */;
/*!40000 ALTER TABLE `GDN_Spammer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_Tag`
--

DROP TABLE IF EXISTS `GDN_Tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_Tag` (
  `TagID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `InsertUserID` int(11) DEFAULT NULL,
  `DateInserted` datetime NOT NULL,
  `CategoryID` int(11) NOT NULL DEFAULT '-1',
  `CountDiscussions` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`TagID`),
  UNIQUE KEY `UX_Tag` (`Name`,`CategoryID`),
  KEY `IX_Tag_Type` (`Type`),
  KEY `FK_Tag_InsertUserID` (`InsertUserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_Tag`
--

LOCK TABLES `GDN_Tag` WRITE;
/*!40000 ALTER TABLE `GDN_Tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `GDN_Tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_TagDiscussion`
--

DROP TABLE IF EXISTS `GDN_TagDiscussion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_TagDiscussion` (
  `TagID` int(11) NOT NULL,
  `DiscussionID` int(11) NOT NULL,
  `CategoryID` int(11) NOT NULL,
  `DateInserted` datetime DEFAULT NULL,
  PRIMARY KEY (`TagID`,`DiscussionID`),
  KEY `IX_TagDiscussion_CategoryID` (`CategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_TagDiscussion`
--

LOCK TABLES `GDN_TagDiscussion` WRITE;
/*!40000 ALTER TABLE `GDN_TagDiscussion` DISABLE KEYS */;
/*!40000 ALTER TABLE `GDN_TagDiscussion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_User`
--

DROP TABLE IF EXISTS `GDN_User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_User` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Password` varbinary(100) NOT NULL,
  `HashMethod` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Location` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `About` text COLLATE utf8_unicode_ci,
  `Email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ShowEmail` tinyint(4) NOT NULL DEFAULT '0',
  `Gender` enum('u','m','f') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'u',
  `CountVisits` int(11) NOT NULL DEFAULT '0',
  `CountInvitations` int(11) NOT NULL DEFAULT '0',
  `CountNotifications` int(11) DEFAULT NULL,
  `InviteUserID` int(11) DEFAULT NULL,
  `DiscoveryText` text COLLATE utf8_unicode_ci,
  `Preferences` text COLLATE utf8_unicode_ci,
  `Permissions` text COLLATE utf8_unicode_ci,
  `Attributes` text COLLATE utf8_unicode_ci,
  `DateSetInvitations` datetime DEFAULT NULL,
  `DateOfBirth` datetime DEFAULT NULL,
  `DateFirstVisit` datetime DEFAULT NULL,
  `DateLastActive` datetime DEFAULT NULL,
  `LastIPAddress` varchar(39) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AllIPAddresses` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateInserted` datetime NOT NULL,
  `InsertIPAddress` varchar(39) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `UpdateIPAddress` varchar(39) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HourOffset` int(11) NOT NULL DEFAULT '0',
  `Score` float DEFAULT NULL,
  `Admin` tinyint(4) NOT NULL DEFAULT '0',
  `Verified` tinyint(4) NOT NULL DEFAULT '0',
  `Banned` tinyint(4) NOT NULL DEFAULT '0',
  `Deleted` tinyint(4) NOT NULL DEFAULT '0',
  `Points` int(11) NOT NULL DEFAULT '0',
  `CountUnreadConversations` int(11) DEFAULT NULL,
  `CountDiscussions` int(11) DEFAULT NULL,
  `CountUnreadDiscussions` int(11) DEFAULT NULL,
  `CountComments` int(11) DEFAULT NULL,
  `CountDrafts` int(11) DEFAULT NULL,
  `CountBookmarks` int(11) DEFAULT NULL,
  PRIMARY KEY (`UserID`),
  KEY `FK_User_Name` (`Name`),
  KEY `IX_User_Email` (`Email`),
  KEY `IX_User_DateLastActive` (`DateLastActive`),
  KEY `IX_User_DateInserted` (`DateInserted`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_User`
--

LOCK TABLES `GDN_User` WRITE;
/*!40000 ALTER TABLE `GDN_User` DISABLE KEYS */;
INSERT INTO `GDN_User` VALUES (1,'Admin','$P$BLOSBSYDam0XKUWfOm2JRd.J5HoBMv/','Vanilla',NULL,NULL,NULL,NULL,'admin@test.com',0,'u',2,0,NULL,NULL,NULL,'a:1:{s:13:\"Authenticator\";s:8:\"password\";}','','a:1:{s:12:\"TransientKey\";s:12:\"GSM3NUDAL0LX\";}',NULL,'1975-09-16 00:00:00','2014-09-14 01:24:37','2014-09-14 05:33:28','192.168.23.1','192.168.23.1','2014-09-14 01:24:37','192.168.23.1','2014-09-14 02:56:59','192.168.23.1',-6,NULL,1,0,0,0,0,NULL,1,NULL,NULL,0,NULL),(2,'System','I5UWBHGQXIWL4L7DIS15','Random','http://192.168.23.23/forum/applications/dashboard/design/images/usericon.png',NULL,NULL,NULL,'system@domain.com',0,'u',0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2014-09-14 01:24:42',NULL,NULL,NULL,0,NULL,2,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `GDN_User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_UserAuthentication`
--

DROP TABLE IF EXISTS `GDN_UserAuthentication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_UserAuthentication` (
  `ForeignUserKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ProviderKey` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `UserID` int(11) NOT NULL,
  PRIMARY KEY (`ForeignUserKey`,`ProviderKey`),
  KEY `FK_UserAuthentication_UserID` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_UserAuthentication`
--

LOCK TABLES `GDN_UserAuthentication` WRITE;
/*!40000 ALTER TABLE `GDN_UserAuthentication` DISABLE KEYS */;
INSERT INTO `GDN_UserAuthentication` VALUES ('1','929014295',1);
/*!40000 ALTER TABLE `GDN_UserAuthentication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_UserAuthenticationNonce`
--

DROP TABLE IF EXISTS `GDN_UserAuthenticationNonce`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_UserAuthenticationNonce` (
  `Nonce` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `Token` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Nonce`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_UserAuthenticationNonce`
--

LOCK TABLES `GDN_UserAuthenticationNonce` WRITE;
/*!40000 ALTER TABLE `GDN_UserAuthenticationNonce` DISABLE KEYS */;
/*!40000 ALTER TABLE `GDN_UserAuthenticationNonce` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_UserAuthenticationProvider`
--

DROP TABLE IF EXISTS `GDN_UserAuthenticationProvider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_UserAuthenticationProvider` (
  `AuthenticationKey` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `AuthenticationSchemeAlias` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `Name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AssociationSecret` text COLLATE utf8_unicode_ci,
  `AssociationHashMethod` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AuthenticateUrl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RegisterUrl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SignInUrl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SignOutUrl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PasswordUrl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ProfileUrl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Attributes` text COLLATE utf8_unicode_ci,
  `IsDefault` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`AuthenticationKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_UserAuthenticationProvider`
--

LOCK TABLES `GDN_UserAuthenticationProvider` WRITE;
/*!40000 ALTER TABLE `GDN_UserAuthenticationProvider` DISABLE KEYS */;
INSERT INTO `GDN_UserAuthenticationProvider` VALUES ('929014295','jsconnect','Eleven',NULL,'35c65fcff73e14cae439ba0764353a02','md5','http://192.168.23.23/vanilla','http://192.168.23.23/register','http://192.168.23.23/login','http://192.168.23.23/logout',NULL,NULL,'a:3:{s:8:\"HashType\";s:3:\"md5\";s:8:\"TestMode\";b:0;s:7:\"Trusted\";s:1:\"1\";}',1);
/*!40000 ALTER TABLE `GDN_UserAuthenticationProvider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_UserAuthenticationToken`
--

DROP TABLE IF EXISTS `GDN_UserAuthenticationToken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_UserAuthenticationToken` (
  `Token` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ProviderKey` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ForeignUserKey` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TokenSecret` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `TokenType` enum('request','access') COLLATE utf8_unicode_ci NOT NULL,
  `Authorized` tinyint(4) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Lifetime` int(11) NOT NULL,
  PRIMARY KEY (`Token`,`ProviderKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_UserAuthenticationToken`
--

LOCK TABLES `GDN_UserAuthenticationToken` WRITE;
/*!40000 ALTER TABLE `GDN_UserAuthenticationToken` DISABLE KEYS */;
/*!40000 ALTER TABLE `GDN_UserAuthenticationToken` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_UserCategory`
--

DROP TABLE IF EXISTS `GDN_UserCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_UserCategory` (
  `UserID` int(11) NOT NULL,
  `CategoryID` int(11) NOT NULL,
  `DateMarkedRead` datetime DEFAULT NULL,
  `Unfollow` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`UserID`,`CategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_UserCategory`
--

LOCK TABLES `GDN_UserCategory` WRITE;
/*!40000 ALTER TABLE `GDN_UserCategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `GDN_UserCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_UserComment`
--

DROP TABLE IF EXISTS `GDN_UserComment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_UserComment` (
  `UserID` int(11) NOT NULL,
  `CommentID` int(11) NOT NULL,
  `Score` float DEFAULT NULL,
  `DateLastViewed` datetime DEFAULT NULL,
  PRIMARY KEY (`UserID`,`CommentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_UserComment`
--

LOCK TABLES `GDN_UserComment` WRITE;
/*!40000 ALTER TABLE `GDN_UserComment` DISABLE KEYS */;
/*!40000 ALTER TABLE `GDN_UserComment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_UserConversation`
--

DROP TABLE IF EXISTS `GDN_UserConversation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_UserConversation` (
  `UserID` int(11) NOT NULL,
  `ConversationID` int(11) NOT NULL,
  `CountReadMessages` int(11) NOT NULL DEFAULT '0',
  `LastMessageID` int(11) DEFAULT NULL,
  `DateLastViewed` datetime DEFAULT NULL,
  `DateCleared` datetime DEFAULT NULL,
  `Bookmarked` tinyint(4) NOT NULL DEFAULT '0',
  `Deleted` tinyint(4) NOT NULL DEFAULT '0',
  `DateConversationUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`UserID`,`ConversationID`),
  KEY `IX_UserConversation_Inbox` (`UserID`,`Deleted`,`DateConversationUpdated`),
  KEY `FK_UserConversation_ConversationID` (`ConversationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_UserConversation`
--

LOCK TABLES `GDN_UserConversation` WRITE;
/*!40000 ALTER TABLE `GDN_UserConversation` DISABLE KEYS */;
INSERT INTO `GDN_UserConversation` VALUES (1,1,0,1,NULL,NULL,0,0,'2014-09-14 01:24:42');
/*!40000 ALTER TABLE `GDN_UserConversation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_UserDiscussion`
--

DROP TABLE IF EXISTS `GDN_UserDiscussion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_UserDiscussion` (
  `UserID` int(11) NOT NULL,
  `DiscussionID` int(11) NOT NULL,
  `Score` float DEFAULT NULL,
  `CountComments` int(11) NOT NULL DEFAULT '0',
  `DateLastViewed` datetime DEFAULT NULL,
  `Dismissed` tinyint(4) NOT NULL DEFAULT '0',
  `Bookmarked` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`UserID`,`DiscussionID`),
  KEY `FK_UserDiscussion_DiscussionID` (`DiscussionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_UserDiscussion`
--

LOCK TABLES `GDN_UserDiscussion` WRITE;
/*!40000 ALTER TABLE `GDN_UserDiscussion` DISABLE KEYS */;
INSERT INTO `GDN_UserDiscussion` VALUES (1,1,NULL,1,'2014-09-14 01:26:16',0,0),(1,2,NULL,0,'2014-09-14 05:34:51',0,0);
/*!40000 ALTER TABLE `GDN_UserDiscussion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_UserMerge`
--

DROP TABLE IF EXISTS `GDN_UserMerge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_UserMerge` (
  `MergeID` int(11) NOT NULL AUTO_INCREMENT,
  `OldUserID` int(11) NOT NULL,
  `NewUserID` int(11) NOT NULL,
  `DateInserted` datetime NOT NULL,
  `InsertUserID` int(11) NOT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `UpdateUserID` int(11) DEFAULT NULL,
  `Attributes` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`MergeID`),
  KEY `FK_UserMerge_OldUserID` (`OldUserID`),
  KEY `FK_UserMerge_NewUserID` (`NewUserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_UserMerge`
--

LOCK TABLES `GDN_UserMerge` WRITE;
/*!40000 ALTER TABLE `GDN_UserMerge` DISABLE KEYS */;
/*!40000 ALTER TABLE `GDN_UserMerge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_UserMergeItem`
--

DROP TABLE IF EXISTS `GDN_UserMergeItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_UserMergeItem` (
  `MergeID` int(11) NOT NULL,
  `Table` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `Column` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `RecordID` int(11) NOT NULL,
  `OldUserID` int(11) NOT NULL,
  `NewUserID` int(11) NOT NULL,
  KEY `FK_UserMergeItem_MergeID` (`MergeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_UserMergeItem`
--

LOCK TABLES `GDN_UserMergeItem` WRITE;
/*!40000 ALTER TABLE `GDN_UserMergeItem` DISABLE KEYS */;
/*!40000 ALTER TABLE `GDN_UserMergeItem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_UserMeta`
--

DROP TABLE IF EXISTS `GDN_UserMeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_UserMeta` (
  `UserID` int(11) NOT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Value` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`UserID`,`Name`),
  KEY `IX_UserMeta_Name` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_UserMeta`
--

LOCK TABLES `GDN_UserMeta` WRITE;
/*!40000 ALTER TABLE `GDN_UserMeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `GDN_UserMeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_UserPoints`
--

DROP TABLE IF EXISTS `GDN_UserPoints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_UserPoints` (
  `SlotType` enum('d','w','m','y','a') COLLATE utf8_unicode_ci NOT NULL,
  `TimeSlot` datetime NOT NULL,
  `Source` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Total',
  `UserID` int(11) NOT NULL,
  `Points` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`SlotType`,`TimeSlot`,`Source`,`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_UserPoints`
--

LOCK TABLES `GDN_UserPoints` WRITE;
/*!40000 ALTER TABLE `GDN_UserPoints` DISABLE KEYS */;
/*!40000 ALTER TABLE `GDN_UserPoints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GDN_UserRole`
--

DROP TABLE IF EXISTS `GDN_UserRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GDN_UserRole` (
  `UserID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  PRIMARY KEY (`UserID`,`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GDN_UserRole`
--

LOCK TABLES `GDN_UserRole` WRITE;
/*!40000 ALTER TABLE `GDN_UserRole` DISABLE KEYS */;
INSERT INTO `GDN_UserRole` VALUES (0,2),(1,16);
/*!40000 ALTER TABLE `GDN_UserRole` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-09-13 23:40:19
